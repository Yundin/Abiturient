package org.styleru.abiturientproject.presenter;

import android.content.Context;
import android.test.mock.MockContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mockito;
import org.styleru.abiturientproject.model.dto.EdProgramItem;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.view.IOnEdProgramClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import ru.arturvasilov.rxloader.LifecycleHandler;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertNotNull;

/**
 * @author Yundin Vladislav
 */
@RunWith(JUnit4.class)
public class EdProgramsPresenterTest {

    private IViewWithData view;
    private EdProgramsPresenter presenter;
    private Context context;

    @Before
    public void setUp() {
        LifecycleHandler lifecycleHandler = Mockito.mock(LifecycleHandler.class);
        view = new IViewWithData() {
            @Override
            public void showError(Throwable throwable) {

            }

            @Override
            public Context getContext() {
                return new MockContext();
            }

            @Override
            public void onDataUpdated(Object data) {

            }

            @Override
            public Object getData() {
                return null;
            }
        };
        IOnEdProgramClickListener listener = Mockito.mock(IOnEdProgramClickListener.class);

        presenter = new EdProgramsPresenter(view, lifecycleHandler, listener);
    }

    @Test
    public void testCreated() throws Exception {
        assertNotNull(presenter);
    }

    @Test
    public void updateDBTest() {
        List<EdProgramItem> data = new ArrayList<>();
        data.add(new EdProgramItem("TestTag", "TestTitle", "TestPlace", "TestLink"));
//        presenter.updateDataBase(data);

        List<Item> abstractData = new ArrayList<>(data);
        Mockito.verify(view).onDataUpdated(abstractData);
    }
}
