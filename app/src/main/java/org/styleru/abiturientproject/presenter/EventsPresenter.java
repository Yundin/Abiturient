package org.styleru.abiturientproject.presenter;

import android.support.annotation.NonNull;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.dto.EventItem;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.view.IOnEventClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.BaseRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class EventsPresenter extends Presenter<List<Item>> implements BaseRecyclerAdapter.OnItemClick {

    private IViewWithData<List<Item>> view;
    private final LifecycleHandler lifecycleHandler;
    private final IOnEventClickListener listener;

    public EventsPresenter(IViewWithData<List<Item>> view, LifecycleHandler lifecycleHandler, IOnEventClickListener listener) {
        super(view);
        this.view = view;
        this.lifecycleHandler = lifecycleHandler;
        this.listener = listener;
    }

    @Override
    public void onItemClick(@NonNull String title) {
        listener.onEventClick(title);
    }

    @Override
    public void updateData() {
        RepositoryProvider.provideAbiturientRepository()
                .getEvents()
                .compose(lifecycleHandler.reload(R.id.events_request_id))
                .subscribe(this::onDataUpdated, view::showError);
    }

    private void onDataUpdated(@NonNull List<EventItem> data) {
        Collections.reverse(data);
        List<Item> castedData = new ArrayList<>(data);
        if (!castedData.equals(view.getData()) && !data.isEmpty()) {
            updateDataBase(data);
        }
        view.onDataUpdated(castedData);
    }

    private void updateDataBase(List<EventItem> data) {
        RepositoryProvider.provideDBRepository()
                .updateEventItems(data, null);
    }

    public List<Item> getData() {
        return RepositoryProvider.provideDBRepository()
                .getEventItems();
    }
}
