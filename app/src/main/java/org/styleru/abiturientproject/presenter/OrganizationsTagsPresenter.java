package org.styleru.abiturientproject.presenter;

import android.support.annotation.NonNull;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.database.HSEContract;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.SquareCardItem;
import org.styleru.abiturientproject.view.IOnOrganizationTagClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.BaseRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class OrganizationsTagsPresenter extends Presenter<List<Item>> implements BaseRecyclerAdapter.OnItemClick {

    private final IOnOrganizationTagClickListener listener;
    private final LifecycleHandler lifecycleHandler;
    private SquareCardsPresenter parentPresenter;

    public OrganizationsTagsPresenter(IViewWithData<List<Item>> view, IOnOrganizationTagClickListener listener, LifecycleHandler lifecycleHandler) {
        super(view);
        this.listener = listener;
        this.lifecycleHandler = lifecycleHandler;
        parentPresenter = new SquareCardsPresenter();
    }

    @Override
    public void onItemClick(@NonNull String title) {
        listener.onOrganizationTagClick(title);
    }

    @Override
    public void updateData() {
        RepositoryProvider.provideAbiturientRepository()
                .getOrganizationsTags()
                .compose(lifecycleHandler.load(R.id.organizations_tags_request_id))
                .subscribe(this::onDataUpdated, getView()::showError);
    }

    public List<Item> getData() {
        String tableName = HSEContract.OrganizationsTagsEntry.TABLE_NAME;
        String[] projection = {
                HSEContract.OrganizationsTagsEntry.COLUMN_TITLE,
                HSEContract.OrganizationsTagsEntry.COLUMN_IMAGE_URL};

        return parentPresenter.getData(tableName, projection);
    }

    private void onDataUpdated(@NonNull List<SquareCardItem> data) {
        List<Item> castedData = new ArrayList<>(data);
        if (!(castedData.equals(getView().getData())) && !data.isEmpty()) {
            updateDataBase(data);
            getView().onDataUpdated(castedData);
        }
    }

    private void updateDataBase(List<SquareCardItem> data) {
        String tableName = HSEContract.OrganizationsTagsEntry.TABLE_NAME;
        String[] projection = {
                HSEContract.OrganizationsTagsEntry.COLUMN_TITLE,
                HSEContract.OrganizationsTagsEntry.COLUMN_IMAGE_URL};

        List<SquareCardItem> castedData = new ArrayList<>(data);
        parentPresenter.updateDataBase(castedData, tableName, projection);
    }
}