package org.styleru.abiturientproject.presenter;

import android.support.annotation.NonNull;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.dto.EdProgramItem;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.view.IOnEdProgramClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.BaseRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class EdProgramsPresenter extends Presenter<List<Item>> implements BaseRecyclerAdapter.OnItemClick {

    private IViewWithData<List<Item>> view;
    private String title;
    private final LifecycleHandler lifecycleHandler;
    private final IOnEdProgramClickListener listener;

    public EdProgramsPresenter(IViewWithData<List<Item>> view, LifecycleHandler lifecycleHandler, IOnEdProgramClickListener listener) {
        super(view);
        this.view = view;
        this.lifecycleHandler = lifecycleHandler;
        this.listener = listener;
    }

    @Override
    public void onItemClick(@NonNull String title) {
        listener.onEdProgramClick(title);
    }

    @Override
    public void updateData() {
        RepositoryProvider.provideAbiturientRepository()
                .getEdPrograms()
                .compose(lifecycleHandler.load(R.id.ed_programs_request_id))
                .subscribe(this::onDataUpdated, view::showError);
    }

    private List<Item> getData() {
        return RepositoryProvider.provideDBRepository()
                .getEdProgramItems();
    }

    public List<Item> getData(String title) {
        this.title = title;
        return RepositoryProvider.provideDBRepository()
                .getEdProgramItemsByTag(title);
    }

    private void onDataUpdated(@NonNull List<EdProgramItem> data) {
        List<Item> castedData = new ArrayList<>(data);
        if (!(castedData.equals(getData())) && !data.isEmpty()) {
            IOnDataUpdatedCallback callback = () -> view.onDataUpdated(getData(title));
            updateDataBase(data, callback);
        }
    }

    public void updateDataBase(List<EdProgramItem> data, IOnDataUpdatedCallback callback) {
        RepositoryProvider.provideDBRepository()
                .updateEdProgramItems(data, callback);
    }

    public String getImageUrl(String title) {
        return RepositoryProvider.provideDBRepository()
                .getFacyltyTagImageByTitle(title);
    }
}
