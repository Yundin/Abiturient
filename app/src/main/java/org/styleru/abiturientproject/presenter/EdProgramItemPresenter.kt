package org.styleru.abiturientproject.presenter

import org.styleru.abiturientproject.R
import org.styleru.abiturientproject.model.RepositoryProvider
import org.styleru.abiturientproject.model.dto.EdProgramDescriptionItem
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.presenter.Presenter.IOnDataUpdatedCallback
import org.styleru.abiturientproject.view.IViewWithData
import ru.arturvasilov.rxloader.LifecycleHandler

/**
 * @author Yundin Vladislav
 */
class EdProgramItemPresenter constructor(view: IViewWithData<List<Item>>,
                                         private val lifecycleHandler: LifecycleHandler) : Presenter<List<Item>>(view) {

    lateinit var title: String

    fun getData(title: String): List<Item>? {

        this.title = title
        return RepositoryProvider.provideDBRepository()
                .getEdProgramsDescriptionItemsByTitle(title)
    }

    override fun updateData() {
        RepositoryProvider.provideAbiturientRepository()
                .edProgramsDescription
                .compose(lifecycleHandler.load(R.id.ed_program_description_request_id))
                .subscribe(this::onDataUpdated, view::showError)
    }

    private fun onDataUpdated(data: List<EdProgramDescriptionItem>) {

        val castedData: List<Item> = ArrayList(data)
        if (!castedData.equals(getData()) && !castedData.isEmpty()) {
            val callback = IOnDataUpdatedCallback { view.onDataUpdated(getData(title)) }
            updateDataBase(data, callback)
        }
    }

    private fun getData(): List<Item>? {
        return RepositoryProvider.provideDBRepository()
                .edProgramsDescriptionItems
    }

    private fun updateDataBase(data: List<EdProgramDescriptionItem>, callback: IOnDataUpdatedCallback) {
        RepositoryProvider.provideDBRepository()
                .updateEdProgramDescriptionItems(data, callback)
    }

    fun getAddress(title: String): String? {

        return RepositoryProvider.provideDBRepository()
                .getEdprogramAddressByTitle(title)
    }

    fun getLink(title: String): String? {

        return RepositoryProvider.provideDBRepository()
                .getEdprogramLinkByTitle(title)
    }
}