package org.styleru.abiturientproject.presenter;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.support.annotation.NonNull;
import org.styleru.abiturientproject.BuildConfig;
import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.OrganizationItem;
import org.styleru.abiturientproject.view.IViewWithData;
import rx.observables.ConnectableObservable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class OrganizationItemPresenter extends Presenter<Item> {

    private IViewWithData<Item> view;
    private String title;
    private final ConnectableObservable<List<OrganizationItem>> connectableObservable;

    private Handler handler = new Handler(msg -> {
        view.onDataUpdated(getData(title));
        return true;
    });

    public OrganizationItemPresenter(IViewWithData<Item> view, ConnectableObservable<List<OrganizationItem>> connectableObservable) {
        super(view);

        this.view = view;
        this.connectableObservable = connectableObservable;
    }

    public void onClick(Item data) {

        OrganizationItem castedData = (OrganizationItem) data;
        try {
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(castedData.getLink()));
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            view.getContext().startActivity(intent);
        } catch (ActivityNotFoundException e) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void updateData() {

        if (connectableObservable != null) {
            connectableObservable.subscribe(this::onDataUpdated, view::showError);
        }
    }

    private void onDataUpdated(@NonNull List<OrganizationItem> data) {

        List<Item> castedData = new ArrayList<>(data);
        if (!(castedData.equals(getOrganizations())) && !data.isEmpty()) {
            IOnDataUpdatedCallback callback = () -> view.onDataUpdated(getData(title));
            updateDataBase(data, callback);
        }
    }

    private void updateDataBase(List<OrganizationItem> data, IOnDataUpdatedCallback callback) {
        RepositoryProvider.provideDBRepository()
                .updateOrganizationItems(data, callback);
    }

    private List<Item> getOrganizations() {
        return RepositoryProvider.provideDBRepository()
                .getOrganizationItems();
    }

    public Item getData(String title) {
        return RepositoryProvider.provideDBRepository()
                .getOrganizationItemByTitle(title);
    }
}
