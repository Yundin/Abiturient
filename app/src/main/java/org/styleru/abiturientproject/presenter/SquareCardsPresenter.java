package org.styleru.abiturientproject.presenter;


import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.SquareCardItem;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class SquareCardsPresenter {

    public List<Item> getData(String tableName, String[] projection) {

        return RepositoryProvider.provideDBRepository()
                .getSquareCardItems(tableName, projection);
    }

    public void updateDataBase(List<SquareCardItem> data, String tableName, String[] fields) {

        RepositoryProvider.provideDBRepository()
                .updateSquareCardItems(tableName, data, fields, null);
    }
}
