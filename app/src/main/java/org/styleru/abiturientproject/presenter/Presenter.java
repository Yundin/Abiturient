package org.styleru.abiturientproject.presenter;

import org.styleru.abiturientproject.view.IViewWithData;

/**
 * @author Yundin Vladislav
 */
public abstract class Presenter<T> {

    private IViewWithData<T> view;

    public Presenter(IViewWithData<T> view) {
        this.view = view;
    }

    public IViewWithData getView() {
        return view;
    }

    public abstract void updateData();

    public interface IOnDataUpdatedCallback {
        void onDataUpdated();
    }
}