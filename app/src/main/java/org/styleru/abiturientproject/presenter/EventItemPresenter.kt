package org.styleru.abiturientproject.presenter

import org.styleru.abiturientproject.model.RepositoryProvider
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.view.IViewWithData

/**
 * @author Yundin Vladislav
 */
class EventItemPresenter(view: IViewWithData<Item>) : Presenter<Item>(view) {

    fun getData(title: String): Item? {

        return RepositoryProvider.provideDBRepository()
                .getEventItemByTitle(title)
    }

    fun getImageUrl(title: String): String? {
        return RepositoryProvider.provideDBRepository()
                .getEventItemImageByTitle(title)
    }

    override fun updateData() {
        //data can't be updated
    }
}