package org.styleru.abiturientproject.presenter;

import android.support.annotation.NonNull;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.OrganizationItem;
import org.styleru.abiturientproject.view.IOnOrganizationClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.BaseRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;
import rx.observables.ConnectableObservable;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class OrganizationsPresenter extends Presenter<List<Item>> implements BaseRecyclerAdapter.OnItemClick {

    private IViewWithData<List<Item>> view;
    private String title;
    private final IOnOrganizationClickListener listener;
    private final LifecycleHandler lifecycleHandler;

    private ConnectableObservable<List<OrganizationItem>> connectableObservable;
    private boolean wasAnswer = false;

    public OrganizationsPresenter(IViewWithData<List<Item>> view, IOnOrganizationClickListener listener,
                                  LifecycleHandler lifecycleHandler) {
        super(view);
        this.view = view;
        this.listener = listener;
        this.lifecycleHandler = lifecycleHandler;
    }

    @Override
    public void onItemClick(@NonNull String title) {

        if (wasAnswer) {
            listener.onOrganizationClick(title);
        } else {
            listener.onOrganizationClick(title, connectableObservable);
        }
    }

    @Override
    public void updateData() {

        connectableObservable = RepositoryProvider.provideAbiturientRepository()
                .getOrganizations()
                .compose(lifecycleHandler.load(R.id.organizations_request_id))
                .replay();

        connectableObservable.subscribe(this::onDataUpdated, view::showError);
        connectableObservable.connect();
    }

    private List<Item> getData() {
        return RepositoryProvider.provideDBRepository()
                .getOrganizationItems();
    }

    public List<Item> getData(String tag) {
        title = tag;
        return RepositoryProvider.provideDBRepository()
                .getOrganizationItemsByTag(tag);
    }

    private void onDataUpdated(@NonNull List<OrganizationItem> data) {
        wasAnswer = true;
        List<Item> castedData = new ArrayList<>(data);
        if (!(castedData.equals(getData())) && !data.isEmpty()) {
            IOnDataUpdatedCallback callback = () -> view.onDataUpdated(getData(title));
            updateDataBase(data, callback);
        }
    }

    private void updateDataBase(List<OrganizationItem> data, IOnDataUpdatedCallback callback) {
        RepositoryProvider.provideDBRepository()
                .updateOrganizationItems(data, callback);
    }

    public String getImageUrl(String title) {
        return RepositoryProvider.provideDBRepository()
                .getOrganizationTagImageByTitle(title);
    }
}
