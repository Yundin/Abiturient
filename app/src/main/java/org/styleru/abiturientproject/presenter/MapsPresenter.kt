package org.styleru.abiturientproject.presenter

import org.styleru.abiturientproject.R
import org.styleru.abiturientproject.model.RepositoryProvider
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.model.dto.MapItem
import org.styleru.abiturientproject.model.dto.StringItem
import org.styleru.abiturientproject.presenter.Presenter.IOnDataUpdatedCallback
import org.styleru.abiturientproject.view.IViewWithData
import ru.arturvasilov.rxloader.LifecycleHandler

/**
 * @author Yundin Vladislav
 */
class MapsPresenter constructor(view: IViewWithData<Item>, private val mapListener: IMapListener,
                                private val lifecycleHandler: LifecycleHandler) : Presenter<Item>(view) {

    override fun updateData() {
        RepositoryProvider.provideAbiturientRepository()
                .maps
                .compose(lifecycleHandler.reload(R.id.maps_request_id))
                .subscribe(this::onDataUpdated, view::showError)
    }

    private fun onDataUpdated(data: List<MapItem>) {

        if (!data.equals(getAllData()) && !data.isEmpty()) {
            val callback = IOnDataUpdatedCallback {
                mapListener.resetMarkers()
                view.onDataUpdated(getData((view.data as StringItem).transferredData))
            }
            updateDataBase(data, callback)
        } else {
            mapListener.onOldOrNullDataReceived()
        }
    }

    private fun updateDataBase(data: List<MapItem>, callback: IOnDataUpdatedCallback) {
        RepositoryProvider.provideDBRepository()
                .updateMapItems(data, callback)
    }

    fun getData(address: String): Item? {

        return RepositoryProvider.provideDBRepository()
                .getMapItemByAddress(address)
    }

    fun getAllData(): List<MapItem>? {

        return RepositoryProvider.provideDBRepository()
                .mapItems
    }

    interface IMapListener {

        fun resetMarkers()
        fun onOldOrNullDataReceived()
    }
}