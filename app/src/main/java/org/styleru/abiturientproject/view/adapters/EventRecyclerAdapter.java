package org.styleru.abiturientproject.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.api.ApiFactory;
import org.styleru.abiturientproject.model.dto.EventItem;
import org.styleru.abiturientproject.model.dto.Item;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class EventRecyclerAdapter extends BaseRecyclerAdapter {

    private final OnItemClick onItemClick;

    private final View.OnClickListener internalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String title = (String) view.getTag();
            onItemClick.onItemClick(title);
        }
    };

    public EventRecyclerAdapter(Context context, List<Item> data, OnItemClick onItemClick) {
        super(context, data);
        this.onItemClick = onItemClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.event_card, parent, false);
        return new EventViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder uncastedHolder, int position) {
        EventItem item = (EventItem) getData().get(position);
        EventViewHolder holder = (EventViewHolder) uncastedHolder;

        holder.title.setText(item.getTitle());

        holder.date.setText(item.getDate());
        holder.location.setText(item.getLocation());
        holder.imageView.setImageResource(R.drawable.placeholder);
        holder.itemView.setTag(item.getTitle());
        holder.itemView.setOnClickListener(internalListener);
        if (item.getImage() != null) {
            Glide
                    .with(context)
                    .load(ApiFactory.BASE_URL + item.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.imageView);
        }
    }

    class EventViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.main_text)
        TextView title;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.location)
        TextView location;
        @BindView(R.id.image)
        ImageView imageView;

        EventViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
