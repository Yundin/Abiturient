package org.styleru.abiturientproject.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.presenter.OrganizationsTagsPresenter;
import org.styleru.abiturientproject.view.IOnOrganizationTagClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.SquareCardsRecyclerAdapter;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class OrganizationsTagsFragment extends SquareCardsFragment implements IViewWithData<List<Item>> {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;

    private OrganizationsTagsPresenter presenter;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_organizations_tags, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();
        presenter.updateData();
    }

    private void init() {

        initToolbar(appBarLayout, toolbar, toolbarTitle, getString(R.string.title_organisations));

        presenter = new OrganizationsTagsPresenter(this, (IOnOrganizationTagClickListener) getActivity(), getLifecycleHandler());
        recyclerView.setLayoutManager(getGridLayoutManager());

        setRecyclerAdapter(new SquareCardsRecyclerAdapter(getContext(), presenter.getData(), presenter));
        recyclerView.setAdapter(getRecyclerAdapter());
    }

    @Override
    public void showError(Throwable throwable) {
        Log.e("OrganizationsTagsFrag..", throwable.getMessage());
    }
}