package org.styleru.abiturientproject.view.fragments

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.r0adkll.slidr.Slidr
import com.r0adkll.slidr.model.SlidrConfig
import com.r0adkll.slidr.model.SlidrInterface
import com.r0adkll.slidr.model.SlidrPosition
import kotlinx.android.synthetic.main.default_toolbar.*
import kotlinx.android.synthetic.main.fragment_ed_program_item.*
import kotlinx.android.synthetic.main.no_data.*
import org.greenrobot.eventbus.EventBus
import org.styleru.abiturientproject.R
import org.styleru.abiturientproject.model.dto.BackPressedEvent
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.presenter.EdProgramItemPresenter
import org.styleru.abiturientproject.view.IGoToMap
import org.styleru.abiturientproject.view.IViewWithData
import org.styleru.abiturientproject.view.adapters.ExpandableListAdapter
import ru.arturvasilov.rxloader.LoaderLifecycleHandler

/**
 * @author Yundin Vladislav
 */
class EdProgramItemFragment : FragmentWithActionbar(), IViewWithData<List<Item>> {

    companion object {
        const val TITLE = "ed_program_item_title"
    }

    private lateinit var title: String
    private lateinit var presenter: EdProgramItemPresenter
    private lateinit var adapter: ExpandableListAdapter
    private var data: List<Item> = ArrayList()
    private var slidrInterface: SlidrInterface? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater!!.inflate(R.layout.fragment_ed_program_item, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
        presenter.updateData()
    }

    override fun onResume() {
        super.onResume()
        if (slidrInterface == null)
            slidrInterface = Slidr.replace(view!!.findViewById(R.id.content_container), SlidrConfig.Builder().position(SlidrPosition.LEFT).build())
    }

    private fun init() {

        title = arguments.getString(TITLE)

        title_text_view.text = title
        go_to_map.setOnClickListener { (activity as IGoToMap).goToMap(presenter.getAddress(title)) }

        val lifecycleHandler = LoaderLifecycleHandler.create(context, activity.supportLoaderManager)

        presenter = EdProgramItemPresenter(this, lifecycleHandler)

        initToolbarWithBackButton(appbar_layout, toolbar, toolbar_title, title)

        title.let {
            presenter.getData(it)?.let {
                data = it
                adapter = ExpandableListAdapter(context, data, presenter.getLink(title))
            }
        }

        checkEmptyData(data)

        list_view.setAdapter(adapter)
    }

    override fun backPressed(): Boolean {
        EventBus.getDefault().post(BackPressedEvent())
        return true
    }

    override fun showError(throwable: Throwable) {
        Log.e("EdProgramItemFragment", throwable.message)
    }

    override fun onDataUpdated(data: List<Item>) {
        adapter.updateDataWithNotify(data)

        checkEmptyData(data)
    }

    override fun getData(): List<Item>? {
        return data
    }

    private fun checkEmptyData(data: List<Item>) {

        if (data.isEmpty()) {
            no_data.visibility = View.VISIBLE
        } else {
            no_data.visibility = View.GONE
        }
    }
}