package org.styleru.abiturientproject.view.fragments;

import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.widget.TextView;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.view.activity.MainActivity;

import static org.styleru.abiturientproject.view.activity.MainActivity.getStatusBarHeight;

/**
 * @author Yundin Vladislav
 */
public abstract class FragmentWithActionbar extends Fragment {

    void initToolbar(AppBarLayout appBarLayout, Toolbar toolbar, TextView toolbarTitle, String title) {

        toolbar.setTitle("");
        toolbarTitle.setText(title);
        appBarLayout.setPadding(0, getStatusBarHeight(getResources()), 0, 0);
    }

    void initToolbarWithBackButton(AppBarLayout appBarLayout, Toolbar toolbar, TextView toolbarTitle, String title) {

        initToolbar(appBarLayout, toolbar, toolbarTitle, title);

        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);

        toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    void initCollapsingToolbar(Toolbar toolbar) {

        toolbar.setTitle("");
        toolbar.setPadding(0, getStatusBarHeight(getResources()), 0, 0);
        TypedValue tv = new TypedValue();
        if (getActivity().getTheme().resolveAttribute(R.attr.actionBarSize, tv, true)) {
            toolbar.getLayoutParams().height = TypedValue.complexToDimensionPixelSize(tv.data, getResources().getDisplayMetrics()) +
                    MainActivity.getStatusBarHeight(getResources());
        }
        toolbar.setNavigationIcon(R.drawable.ic_arrow_back);

        toolbar.setNavigationOnClickListener(v -> getActivity().onBackPressed());
    }

    public abstract boolean backPressed();
}