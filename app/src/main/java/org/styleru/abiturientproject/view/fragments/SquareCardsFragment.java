package org.styleru.abiturientproject.view.fragments;

import android.support.v7.widget.GridLayoutManager;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.SquareCardsRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public abstract class SquareCardsFragment extends FragmentWithActionbar implements IViewWithData<List<Item>> {

    private static GridLayoutManager gridLayoutManager;
    private SquareCardsRecyclerAdapter recyclerAdapter;
    private LifecycleHandler lifecycleHandler;

    public GridLayoutManager getGridLayoutManager() {
        //wide last card if its count is odd
        gridLayoutManager = new GridLayoutManager(getContext(), 2);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (recyclerAdapter.getItemCount() % 2 == 1 && position == recyclerAdapter.getItemCount() - 1) {
                    return 2;
                } else {
                    return 1;
                }
            }
        });
        return gridLayoutManager;
    }

    public LifecycleHandler getLifecycleHandler() {
        if (lifecycleHandler == null) {
            lifecycleHandler = LoaderLifecycleHandler.create(getContext(), getActivity().getSupportLoaderManager());
        }
        return lifecycleHandler;
    }

    public SquareCardsRecyclerAdapter getRecyclerAdapter() {
        return recyclerAdapter;
    }

    public void setRecyclerAdapter(SquareCardsRecyclerAdapter recyclerAdapter) {
        this.recyclerAdapter = recyclerAdapter;
    }

    @Override
    public void showError(Throwable throwable) {

    }

    @Override
    public void onDataUpdated(List<Item> data) {
        recyclerAdapter.updateDataWithNotify(data);
    }

    @Override
    public List<Item> getData() {
        return recyclerAdapter.getData();
    }

    @Override
    public boolean backPressed() {
        return false;
    }
}
