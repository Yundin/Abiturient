package org.styleru.abiturientproject.view

/**
 * @author Yundin Vladislav
 */
interface IOnEdProgramClickListener {

    fun onEdProgramClick(title: String)
}