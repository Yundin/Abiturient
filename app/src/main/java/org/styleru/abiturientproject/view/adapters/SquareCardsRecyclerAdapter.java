package org.styleru.abiturientproject.view.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.api.ApiFactory;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.SquareCardItem;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class SquareCardsRecyclerAdapter extends BaseRecyclerAdapter {

    private final OnItemClick onItemClick;

    private final View.OnClickListener internalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String title = (String) view.getTag();
            onItemClick.onItemClick(title);
        }
    };

    public SquareCardsRecyclerAdapter(Context context, @NonNull List<Item> data, @NonNull OnItemClick onItemClick) {
        super(context, data);
        this.onItemClick = onItemClick;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.square_card, parent, false);
        return new SquareCardsRecyclerAdapter.SquareCardViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder uncastedHolder, int position) {
        SquareCardItem item = (SquareCardItem) getData().get(position);
        SquareCardViewHolder holder = (SquareCardViewHolder) uncastedHolder;

        holder.itemView.setTag(item.getTitle());
        holder.itemView.setOnClickListener(internalListener);
        holder.text.setText(item.getTitle());
        Glide
                .with(context)
                .load(ApiFactory.BASE_URL + item.getImageUrl())
                .error(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.image);
    }


    class SquareCardViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.image)
        ImageView image;

        @BindView(R.id.text)
        TextView text;

        SquareCardViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
