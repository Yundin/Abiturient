package org.styleru.abiturientproject.view;

/**
 * @author Yundin Vladislav
 */
public interface IGoToMap {

    void goToMap(String address);
}
