package org.styleru.abiturientproject.view;

import android.content.Context;

/**
 * @author Yundin Vladislav
 */
public interface IViewWithData<T> {

    void showError(Throwable throwable);

    Context getContext();

    void onDataUpdated(T data);

    T getData();
}
