package org.styleru.abiturientproject.view.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.RelativeLayout
import android.widget.TextView
import org.styleru.abiturientproject.R
import org.styleru.abiturientproject.model.dto.EdProgramDescriptionItem
import org.styleru.abiturientproject.model.dto.Item

/**
 * @author Yundin Vladislav
 */
class ExpandableListAdapter constructor(private var context: Context, private var data: List<Item>, private var link: String?) : BaseExpandableListAdapter() {

    override fun getGroup(groupPosition: Int): Any {
        return (data[groupPosition] as EdProgramDescriptionItem).title
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int): Boolean {
        return false
    }

    override fun hasStableIds(): Boolean {
        return true
    }

    override fun getChildrenCount(groupPosition: Int): Int {
        return 1
    }

    override fun getChild(groupPosition: Int, childPosition: Int): Any {
        return (data[groupPosition] as EdProgramDescriptionItem).text
    }

    override fun getGroupId(groupPosition: Int): Long {
        return groupPosition.toLong()
    }

    override fun getChildId(groupPosition: Int, childPosition: Int): Long {
        return childPosition.toLong()
    }

    override fun getGroupCount(): Int {
        return data.size
    }

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {

        var _convertView: View? = convertView

        if (_convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            _convertView = inflater.inflate(R.layout.group_view, null)
        }

        val layout = _convertView!!.findViewById<RelativeLayout>(R.id.description_layout)

        if (isExpanded) {
            layout.background = context.resources.getDrawable(R.drawable.collapsing_group_bg_stroke, null)
        } else {
            layout.background = context.resources.getDrawable(R.drawable.collapsing_group_bg_stroke_expanded, null)
        }

        val textView = _convertView.findViewById<TextView>(R.id.textGroup)
        textView?.text = (data[groupPosition] as EdProgramDescriptionItem).title

        return _convertView
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {

        var _convertView: View? = convertView

        if (_convertView == null) {
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            _convertView = inflater.inflate(R.layout.child_view, null)
        }

        val textView = _convertView!!.findViewById<TextView>(R.id.textChild)
        var text = (data[groupPosition] as EdProgramDescriptionItem).text
        text = text.replace("\\n", System.getProperty("line.separator"))
        text = text.replace("\\r", "")
        val isLastGroup = (groupPosition == data.size - 1)
        if (isLastGroup) {
            link?.let {
                text += "\nПодробнее от образовательной программе на сайте: $link"
            }
        }
        textView.text = text
        return _convertView
    }

    fun updateDataWithNotify(data: List<Item>) {

        this.data = data
        notifyDataSetChanged()
    }
}