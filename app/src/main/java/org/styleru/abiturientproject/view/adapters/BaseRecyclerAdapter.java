package org.styleru.abiturientproject.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import org.jetbrains.annotations.NotNull;
import org.styleru.abiturientproject.model.dto.Item;

import java.util.Collections;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public abstract class BaseRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    final Context context;
    final LayoutInflater inflater;
    private List<Item> data;

    BaseRecyclerAdapter(Context context, List<Item> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public List<Item> getData() {
        return data;
    }

    public void setData(List<Item> data) {
        this.data = data;
    }

    private void setDataWithNotify(List<Item> data) {
        this.data = data;
        notifyDataSetChanged();
    }

    public void updateDataWithNotify(List<Item> data) {
        if (this.data.equals(Collections.emptyList()) || !this.data.containsAll(data) || this.data.size() != data.size()) {
            setDataWithNotify(data);
        }
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public interface OnItemClick {

        void onItemClick(@NotNull String title);

    }
}
