package org.styleru.abiturientproject.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrInterface;
import com.r0adkll.slidr.model.SlidrPosition;
import org.greenrobot.eventbus.EventBus;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.api.ApiFactory;
import org.styleru.abiturientproject.model.dto.BackPressedEvent;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.OrganizationItem;
import org.styleru.abiturientproject.presenter.OrganizationItemPresenter;
import org.styleru.abiturientproject.view.IViewWithData;
import rx.observables.ConnectableObservable;

import java.util.List;
import java.util.Objects;

/**
 * @author Yundin Vladislav
 */
public class OrganizationItemFragment extends FragmentWithActionbar implements IViewWithData<Item> {

    public static final String TITLE = "organization_title";
    @BindView(R.id.title)
    TextView titleView;
    @BindView(R.id.site)
    TextView siteView;
    @BindView(R.id.main_text)
    TextView mainTextView;
    @BindView(R.id.image)
    ImageView imageView;
    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    private OrganizationItemPresenter presenter;
    private String title;
    private Item data;
    private SlidrInterface slidrInterface;

    private ConnectableObservable<List<OrganizationItem>> connectableObservable = null;

    @OnClick(R.id.connect_button)
    public void callPresenter() {
        presenter.onClick(data);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_organization_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();
        presenter.updateData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (slidrInterface == null)
            slidrInterface = Slidr.replace(Objects.requireNonNull(getView()).findViewById(R.id.content_container), new SlidrConfig.Builder().position(SlidrPosition.LEFT).build());
    }

    private void init() {

        title = getArguments().getString(OrganizationItemFragment.TITLE);

        presenter = new OrganizationItemPresenter(this, connectableObservable);

        initToolbarWithBackButton(appBarLayout, toolbar, toolbarTitle, title);

        data = presenter.getData(title);
        onDataUpdated(data);
    }

    @Override
    public boolean backPressed() {
        EventBus.getDefault().post(new BackPressedEvent());
        return true;
    }

    @Override
    public void showError(Throwable throwable) {
        Log.e("OrganizationItemFragm..", throwable.getMessage());
    }

    @Override
    public void onDataUpdated(Item data) {

        OrganizationItem item = (OrganizationItem) data;

        titleView.setText(item.getTitle());
        siteView.setText(item.getLink());
        mainTextView.setText(item.getDescription());
        Glide
                .with(getContext())
                .load(ApiFactory.BASE_URL + item.getImgUrl())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);
    }

    @Override
    public Item getData() {
        return data;
    }

    public void initObservable(ConnectableObservable<List<OrganizationItem>> connectableObservable) {
        this.connectableObservable = connectableObservable;
    }
}
