package org.styleru.abiturientproject.view.activity;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.jetbrains.annotations.NotNull;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.dto.BackPressedEvent;
import org.styleru.abiturientproject.model.dto.OrganizationItem;
import org.styleru.abiturientproject.view.*;
import org.styleru.abiturientproject.view.fragments.*;
import rx.observables.ConnectableObservable;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class MainActivity extends AppCompatActivity implements IOnFacultyTagClickListener,
        IOnOrganizationTagClickListener, IOnOrganizationClickListener, IOnEventClickListener,
        IOnEdProgramClickListener, IGoToMap {

    private BottomNavigationViewHelper helper;
    private FacultiesTagsFragment facultiesTagsFragment;
    private OrganizationsTagsFragment organizationsTagsFragment;
    private EventsFragment eventsFragment;
    private MapsFragment mapsFragment;

    @BindView(R.id.navigation)
    BottomNavigationView navigation;

    private final BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_events:
                    selectNavigationItem(R.id.navigation_events);
                    if (eventsFragment == null) {
                        eventsFragment = new EventsFragment();
                    }
                    replaceRootFragment(eventsFragment);
                    return true;
                case R.id.navigation_faculty:
                    selectNavigationItem(R.id.navigation_faculty);
                    if (facultiesTagsFragment == null) {
                        facultiesTagsFragment = new FacultiesTagsFragment();
                    }
                    replaceRootFragment(facultiesTagsFragment);
                    return true;
                case R.id.navigation_organisations:
                    selectNavigationItem(R.id.navigation_organisations);
                    if (organizationsTagsFragment == null) {
                        organizationsTagsFragment = new OrganizationsTagsFragment();
                    }
                    replaceRootFragment(organizationsTagsFragment);
                    return true;
                case R.id.navigation_maps:
                    selectNavigationItem(R.id.navigation_maps);
                    if (mapsFragment == null) {
                        mapsFragment = new MapsFragment();
                    }
                    replaceRootFragment(mapsFragment);
                    return true;
            }
            return false;
        }

    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();
        if (getSupportFragmentManager().findFragmentById(R.id.container) == null) {
            navigation.setSelectedItemId(R.id.navigation_faculty);
        } else {
            autoSelectNavigationItem();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    private void init() {

        ButterKnife.bind(this);
        //disable shifting
        helper = new BottomNavigationViewHelper(this);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        BottomNavigationViewHelper.disableShiftMode(navigation);
    }

    public static int getStatusBarHeight(@NotNull Resources resources) {
        int result = 0;
        int resourceId = resources.getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = resources.getDimensionPixelSize(resourceId);
        }
        return result;
    }

    @Override
    public void onFacultyTagClick(@NotNull String title) {
        EdProgramsFragment edProgramsFragment = new EdProgramsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EdProgramsFragment.TITLE, title);
        edProgramsFragment.setArguments(bundle);
        addFragment(edProgramsFragment);
    }

    @Override
    public void onOrganizationTagClick(@NotNull String title) {
        OrganizationsFragment organizationsFragment = new OrganizationsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(OrganizationsFragment.TITLE, title);
        organizationsFragment.setArguments(bundle);
        addFragment(organizationsFragment);
    }

    @Override
    public void onOrganizationClick(@NotNull String title) {

        OrganizationItemFragment organizationItemFragment = new OrganizationItemFragment();
        Bundle bundle = new Bundle();
        bundle.putString(OrganizationItemFragment.TITLE, title);
        organizationItemFragment.setArguments(bundle);
        addFragment(organizationItemFragment);
    }

    @Override
    public void onOrganizationClick(@NotNull String title, ConnectableObservable<List<OrganizationItem>> connectableObservable) {

        OrganizationItemFragment organizationItemFragment = new OrganizationItemFragment();
        organizationItemFragment.initObservable(connectableObservable);

        Bundle bundle = new Bundle();
        bundle.putString(OrganizationItemFragment.TITLE, title);
        organizationItemFragment.setArguments(bundle);
        addFragment(organizationItemFragment);
    }

    @Override
    public void onEventClick(@NotNull String title) {

        EventItemFragment eventItemFragment = new EventItemFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EventItemFragment.TITLE, title);
        eventItemFragment.setArguments(bundle);
        addFragment(eventItemFragment);
    }

    @Override
    public void onEdProgramClick(@NotNull String title) {

        EdProgramItemFragment edProgramItemFragment = new EdProgramItemFragment();
        Bundle bundle = new Bundle();
        bundle.putString(EdProgramItemFragment.TITLE, title);
        edProgramItemFragment.setArguments(bundle);
        addFragment(edProgramItemFragment);
    }

    @Override
    public void goToMap(String address) {

        selectNavigationItem(R.id.navigation_maps);
        mapsFragment = new MapsFragment();
        Bundle bundle = new Bundle();
        bundle.putString(MapsFragment.TITLE, address);
        mapsFragment.setArguments(bundle);
        addFragment(mapsFragment);
    }


    private void replaceRootFragment(@NotNull Fragment fragment) {

        clearBackStack();
        getSupportFragmentManager().beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .replace(R.id.content, fragment)
                .commit();
    }

    private void addFragment(@NotNull Fragment fragment) {

        getSupportFragmentManager().beginTransaction()
                .setCustomAnimations(R.animator.slide_from_left, R.animator.scale_down_fade, R.animator.scale_up_unfade, R.animator.slide_to_right_fade)
                .add(R.id.content, fragment)
                .addToBackStack(null)
                .commit();
    }

    @Subscribe
    public void onBackPressedEvent(@NotNull BackPressedEvent event) {

        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void onBackPressed() {

        FragmentWithActionbar fragment = (FragmentWithActionbar) getSupportFragmentManager().findFragmentById(R.id.content);
        if (!fragment.backPressed()) {
            super.onBackPressed();
        }
    }

    public void selectNavigationItem(int id) {
        helper.onItemSelected(navigation, id);
    }

    private void autoSelectNavigationItem() {

        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.content);
        if (fragment instanceof EventsFragment || fragment instanceof EventItemFragment) {
            selectNavigationItem(R.id.navigation_events);
        } else if (fragment instanceof FacultiesTagsFragment || fragment instanceof EdProgramsFragment || fragment instanceof EdProgramItemFragment) {
            selectNavigationItem(R.id.navigation_faculty);
        } else if (fragment instanceof OrganizationsTagsFragment || fragment instanceof OrganizationsFragment || fragment instanceof OrganizationItemFragment) {
            selectNavigationItem(R.id.navigation_organisations);
        } else {
            selectNavigationItem(R.id.navigation_maps);
        }
    }

    public int getBackStackEntryCount() {
        return getSupportFragmentManager().getBackStackEntryCount();
    }

    private void clearBackStack() {
        getSupportFragmentManager().popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
    }
}