package org.styleru.abiturientproject.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.r0adkll.slidr.Slidr;
import com.r0adkll.slidr.model.SlidrConfig;
import com.r0adkll.slidr.model.SlidrInterface;
import com.r0adkll.slidr.model.SlidrPosition;
import org.greenrobot.eventbus.EventBus;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.api.ApiFactory;
import org.styleru.abiturientproject.model.dto.BackPressedEvent;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.presenter.OrganizationsPresenter;
import org.styleru.abiturientproject.view.IOnOrganizationClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.OrganizationsRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

import java.util.List;
import java.util.Objects;

/**
 * @author Yundin Vladislav
 */
public class OrganizationsFragment extends FragmentWithActionbar implements IViewWithData<List<Item>> {

    public static final String TITLE = "organization_tag_title";
    @BindView(R.id.title)
    TextView titleTextView;
    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar_layout)
    CollapsingToolbarLayout CollapsingToolbar;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.image)
    ImageView imageView;
    private String title;
    private OrganizationsRecyclerAdapter recyclerAdapter;
    private OrganizationsPresenter presenter;
    private SlidrInterface slidrInterface;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_organizations, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();
        presenter.updateData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (slidrInterface == null)
            slidrInterface = Slidr.replace(Objects.requireNonNull(getView()).findViewById(R.id.content_container), new SlidrConfig.Builder().position(SlidrPosition.LEFT).build());
    }

    private void init() {

        final LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());

        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(getContext(), getActivity().getSupportLoaderManager());

        presenter = new OrganizationsPresenter(this, (IOnOrganizationClickListener) getActivity(), lifecycleHandler);

        initCollapsingToolbar();

        recyclerView.setLayoutManager(linearLayoutManager);
        List<Item> data = presenter.getData(title);
        recyclerAdapter = new OrganizationsRecyclerAdapter(getContext(), data, presenter);
        recyclerView.setAdapter(recyclerAdapter);
        checkEmptyData(data);
    }

    private void initCollapsingToolbar() {

        title = getArguments().getString(OrganizationsFragment.TITLE);
        titleTextView.setText(title);

        super.initCollapsingToolbar(toolbar);

        Glide
                .with(getContext())
                .load(ApiFactory.BASE_URL + presenter.getImageUrl(title))
                .dontAnimate()
                .error(R.drawable.placeholder)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);
    }

    @Override
    public boolean backPressed() {
        EventBus.getDefault().post(new BackPressedEvent());
        return true;
    }

    @Override
    public List<Item> getData() {
        return recyclerAdapter.getData();
    }

    @Override
    public void showError(Throwable throwable) {
        Log.e("OrganizationsFragment", throwable.getMessage());
    }

    @Override
    public void onDataUpdated(List<Item> data) {
        recyclerAdapter.updateDataWithNotify(data);

        checkEmptyData(data);
    }

    private void checkEmptyData(List<Item> data) {

        TextView noData = Objects.requireNonNull(getView()).findViewById(R.id.no_data);
        if (data.isEmpty()) {
            noData.setVisibility(View.VISIBLE);
        } else {
            noData.setVisibility(View.GONE);
        }
    }
}