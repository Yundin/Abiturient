package org.styleru.abiturientproject.view;

/**
 * @author Yundin Vladislav
 */
public interface IOnEventClickListener {

    void onEventClick(String title);
}
