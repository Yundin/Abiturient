package org.styleru.abiturientproject.view.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.presenter.EventsPresenter;
import org.styleru.abiturientproject.view.IOnEventClickListener;
import org.styleru.abiturientproject.view.IViewWithData;
import org.styleru.abiturientproject.view.adapters.EventRecyclerAdapter;
import ru.arturvasilov.rxloader.LifecycleHandler;
import ru.arturvasilov.rxloader.LoaderLifecycleHandler;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class EventsFragment extends FragmentWithActionbar implements IViewWithData<List<Item>>, SwipeRefreshLayout.OnRefreshListener {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;
    @BindView(R.id.appbar_layout)
    AppBarLayout appBarLayout;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.toolbar_title)
    TextView toolbarTitle;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefreshLayout;

    private EventRecyclerAdapter recyclerAdapter;

    private EventsPresenter presenter;

    @Override
    public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container,
                                          Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_events, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        init();
        presenter.updateData();
    }

    private void init() {

        initToolbar(appBarLayout, toolbar, toolbarTitle, getString(R.string.title_events));

        //wide last card if its count is odd
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 1);

        LifecycleHandler lifecycleHandler = LoaderLifecycleHandler.create(getContext(), getActivity().getSupportLoaderManager());

        presenter = new EventsPresenter(this, lifecycleHandler, (IOnEventClickListener) getActivity());
        recyclerView.setLayoutManager(gridLayoutManager);

        recyclerAdapter = new EventRecyclerAdapter(getContext(), presenter.getData(), presenter);
        recyclerView.setAdapter(recyclerAdapter);

        swipeRefreshLayout.setOnRefreshListener(this);
    }

    @Override
    public void onRefresh() {
        presenter.updateData();
    }

    @Override
    public boolean backPressed() {
        return false;
    }

    @Override
    public void showError(Throwable throwable) {
        swipeRefreshLayout.setRefreshing(false);
        if (getActivity() != null && isAdded()) {
            Toast.makeText(getActivity().getApplicationContext(), R.string.no_connection, Toast.LENGTH_SHORT).show();
        }
        Log.e("EventsFragment", throwable.getMessage());
    }

    @Override
    public void onDataUpdated(List<Item> data) {
        swipeRefreshLayout.setRefreshing(false);
        recyclerAdapter.updateDataWithNotify(data);
    }

    @Override
    public List<Item> getData() {
        return recyclerAdapter.getData();
    }
}
