package org.styleru.abiturientproject.view.fragments

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.r0adkll.slidr.Slidr
import com.r0adkll.slidr.model.SlidrConfig
import com.r0adkll.slidr.model.SlidrInterface
import com.r0adkll.slidr.model.SlidrPosition
import kotlinx.android.synthetic.main.content_event_item.*
import kotlinx.android.synthetic.main.fragment_event_item.*
import org.greenrobot.eventbus.EventBus
import org.jetbrains.anko.support.v4.browse
import org.styleru.abiturientproject.R
import org.styleru.abiturientproject.model.api.ApiFactory
import org.styleru.abiturientproject.model.dto.BackPressedEvent
import org.styleru.abiturientproject.model.dto.EventItem
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.presenter.EventItemPresenter
import org.styleru.abiturientproject.view.IViewWithData

/**
 * @author Yundin Vladislav
 */

class EventItemFragment : FragmentWithActionbar(), IViewWithData<Item> {

    companion object {
        const val TITLE = "event_item_title"
    }

    private lateinit var presenter: EventItemPresenter
    private var data: Item? = null
    private var title: String? = null
    private var slidrInterface: SlidrInterface? = null

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        return inflater!!.inflate(R.layout.fragment_event_item, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        init()
    }

    override fun onResume() {
        super.onResume()
        if (slidrInterface == null)
            slidrInterface = Slidr.replace(view!!.findViewById(R.id.content_container), SlidrConfig.Builder().position(SlidrPosition.LEFT).build())
    }

    private fun init() {

        presenter = EventItemPresenter(this)

        initCollapsingToolbar()

        title?.let { data = presenter.getData(it) }
        data?.let { onDataUpdated(it) }
    }

    private fun initCollapsingToolbar() {

        title = arguments.getString(EventItemFragment.TITLE)

        super.initCollapsingToolbar(toolbar)

        initDimens()

        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = ""
        actionBar?.setDisplayHomeAsUpEnabled(true)

        title?.let {
            presenter.getImageUrl(it)?.let {
                Glide
                        .with(context)
                        .load(ApiFactory.BASE_URL + it)
                        .dontAnimate()
                        .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                        .into(image)
            }
        }
    }

    private fun initDimens() {
//        val bottomNavigationHeight = resources.getDimensionPixelSize(R.dimen.bottom_navigation_height)
        val navBarHeight = resources.getDimensionPixelSize(resources.getIdentifier("navigation_bar_height", "dimen", "android"))
        parent_layout.setPadding(0, 0, 0, navBarHeight)
    }

    override fun backPressed(): Boolean {
        EventBus.getDefault().post(BackPressedEvent())
        return true
    }

    override fun showError(throwable: Throwable) {
        Log.e("EventItemFragment", throwable.message)
    }

    override fun onDataUpdated(data: Item) {

        val item = data as EventItem
        title_text_view.text = item.title
        main_text_view.text = item.text
        date.text = item.date
        location.text = item.location
        register_button.setOnClickListener { browse(data.link) }
    }

    override fun getData(): Item? {
        return data
    }
}