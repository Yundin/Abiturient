package org.styleru.abiturientproject.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.api.ApiFactory;
import org.styleru.abiturientproject.model.dto.Item;
import org.styleru.abiturientproject.model.dto.OrganizationItem;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class OrganizationsRecyclerAdapter extends BaseRecyclerAdapter {

    private final OnItemClick onItemClick;

    private final View.OnClickListener internalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String title = (String) view.getTag();
            onItemClick.onItemClick(title);
        }
    };

    public OrganizationsRecyclerAdapter(Context context, List<Item> data, OnItemClick onItemClick) {
        super(context, data);
        this.onItemClick = onItemClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.organization_card, parent, false);
        return new OrganizationsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder uncastedHolder, int position) {
        OrganizationItem item = (OrganizationItem) getData().get(position);
        OrganizationsViewHolder holder = (OrganizationsViewHolder) uncastedHolder;

        holder.text.setText(item.getTitle());
        holder.itemView.setTag(item.getTitle());
        holder.itemView.setOnClickListener(internalListener);
        Glide
                .with(context)
                .load(ApiFactory.BASE_URL + item.getImgUrl())
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(holder.logo);
    }

    class OrganizationsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.logo)
        ImageView logo;

        OrganizationsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
