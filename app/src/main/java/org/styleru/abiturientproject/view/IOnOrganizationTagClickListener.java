package org.styleru.abiturientproject.view;

/**
 * @author Yundin Vladislav
 */
public interface IOnOrganizationTagClickListener {

    void onOrganizationTagClick(String title);
}
