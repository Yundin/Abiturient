package org.styleru.abiturientproject.view;

import android.content.Context;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import org.styleru.abiturientproject.R;

import java.lang.reflect.Field;

/**
 * @author Yundin Vladislav
 */
public class BottomNavigationViewHelper {

    private final Context context;

    public BottomNavigationViewHelper(Context context) {
        this.context = context;
    }

    public static void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        }
    }

    public void onItemSelected(BottomNavigationView view, int id) {
        //changing colors
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        for (int i = 0; i < menuView.getChildCount(); i++) {
            BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
            if (item.getId() != id) {
                //noinspection RestrictedApi
                item.setIconTintList(ContextCompat.getColorStateList(context, R.color.bottom_nav_item));
                //noinspection RestrictedApi
                item.setTextColor(ContextCompat.getColorStateList(context, R.color.bottom_nav_item));
            } else {
                //noinspection RestrictedApi
                item.setIconTintList(ContextCompat.getColorStateList(context, R.color.white));
                //noinspection RestrictedApi
                item.setTextColor(ContextCompat.getColorStateList(context, R.color.white));
            }
        }
    }
}
