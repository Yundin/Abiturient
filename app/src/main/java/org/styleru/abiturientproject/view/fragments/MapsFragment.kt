package org.styleru.abiturientproject.view.fragments

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.SearchView
import android.support.v7.widget.TintTypedArray.obtainStyledAttributes
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import android.widget.Toast
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import kotlinx.android.synthetic.main.fragment_maps.*
import org.greenrobot.eventbus.EventBus
import org.styleru.abiturientproject.AbiturientProject.MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION
import org.styleru.abiturientproject.R
import org.styleru.abiturientproject.model.dto.BackPressedEvent
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.model.dto.MapItem
import org.styleru.abiturientproject.model.dto.StringItem
import org.styleru.abiturientproject.presenter.MapsPresenter
import org.styleru.abiturientproject.view.IViewWithData
import org.styleru.abiturientproject.view.activity.MainActivity
import ru.arturvasilov.rxloader.LoaderLifecycleHandler
import java.util.*
import kotlin.collections.ArrayList

/**
 * @author Yundin Vladislav
 */
class MapsFragment : FragmentWithActionbar(), IViewWithData<Item>, OnMapReadyCallback, MapsPresenter.IMapListener, GoogleMap.InfoWindowAdapter {

    companion object {
        const val TITLE = "maps_title"
        private const val DEFAULT_TITLE = "ул. Мясницкая д. 20"
    }

    private var needLocation = false

    private lateinit var presenter: MapsPresenter
    private lateinit var data: Item
    private var mMap: GoogleMap? = null
    private lateinit var adapter: ArrayAdapter<String>
    private lateinit var searchItem: MenuItem
    private var receivedTitle: String = DEFAULT_TITLE
    private var infoInitialized: Boolean = false
    private var mapReady: Boolean = false
    private var notEnoughData: Boolean = false

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        infoInitialized = false
        mapReady = false
        notEnoughData = false
        return inflater!!.inflate(R.layout.fragment_maps, container, false)
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        init()
        presenter.updateData()
    }

    private fun init() {

        checkPermission()

        (childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment).getMapAsync(this)

        arguments?.let {
            it.getString(MapsFragment.TITLE)?.let {
                receivedTitle = it
            }
        }

        val lifecycleHandler = LoaderLifecycleHandler.create(context, activity.supportLoaderManager)
        presenter = MapsPresenter(this, this, lifecycleHandler)

        initToolbar(appbar_layout, toolbar, toolbar_title, getString(R.string.title_maps))

        presenter.getData(receivedTitle)?.let {
            onDataUpdated(it)
        }

        if (!this::data.isInitialized) {

            notEnoughData = true
            presenter.getData(DEFAULT_TITLE)?.let {
                onDataUpdated(it)
            }
        } else if (!infoInitialized) {
            onDataUpdated(data)
        }

        address_layout.setOnClickListener {
            infoInitialized = false
            onDataUpdated(data)
        }

        initSearch()
    }

    private fun initSearch() {
        toolbar.inflateMenu(R.menu.menu_map)
        searchItem = toolbar.menu.findItem(R.id.action_search)
        val searchView = searchItem.actionView as SearchView

        searchItem.setOnActionExpandListener(object : MenuItem.OnActionExpandListener {

            override fun onMenuItemActionCollapse(item: MenuItem): Boolean {
                val cx = list_view.right
                val cy = list_view.top
                val initialRadius = list_view.width
                val anim = ViewAnimationUtils.createCircularReveal(list_view, cx, cy, initialRadius.toFloat(), 0f)

                anim.addListener(object : AnimatorListenerAdapter() {
                    override fun onAnimationEnd(animation: Animator) {
                        super.onAnimationEnd(animation)
                        list_view.visibility = View.INVISIBLE
                    }
                })
                anim.start()
                return true
            }

            override fun onMenuItemActionExpand(item: MenuItem): Boolean {
                val cx = list_view.right
                val cy = list_view.top
                val finalRadius = Math.max(list_view.width, list_view.height)
                val anim = ViewAnimationUtils.createCircularReveal(list_view, cx, cy, 0f, finalRadius.toFloat())
                list_view.visibility = View.VISIBLE
                anim.start()

                val data: MutableList<String> = ArrayList()
                presenter.getAllData()?.forEach {
                    data.add(it.address)
                }
                adapter.clear()
                adapter.addAll(data)
                changeListViewHeight()
                return true
            }
        })

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {

            override fun onQueryTextChange(newText: String?): Boolean {

                val tempData: MutableList<String> = ArrayList()
                presenter.getAllData()?.forEach {
                    if (it.address.contains(newText as CharSequence, true)) {
                        tempData.add(it.address)
                    }
                }

                adapter.clear()
                adapter.addAll(tempData)
                changeListViewHeight()
                return true
            }

            override fun onQueryTextSubmit(query: String?): Boolean {
                return true
            }
        })
        val data: MutableList<String> = ArrayList()
        presenter.getAllData()?.forEach {
            data.add(it.address)
        }

        adapter = ArrayAdapter(context, android.R.layout.simple_expandable_list_item_1, data)
        list_view.adapter = adapter
        list_view.setOnItemClickListener { _, _, position, _ ->
            run {
                infoInitialized = false
                onDataUpdated(presenter.getData(list_view.getItemAtPosition(position).toString()))
                searchItem.collapseActionView()
            }
        }
        list_view.divider = null
    }

    private fun changeListViewHeight() {
        val params = list_view.layoutParams
        val singleHeight = getAttributeDimen(context, android.R.attr.listPreferredItemHeight)
        params.height = singleHeight * adapter.count
        list_view.layoutParams = params
    }

    private fun getAttributeDimen(context: Context, attributeId: Int): Int {

        val attrs = IntArray(1) { attributeId }
        val ta = obtainStyledAttributes(context, R.style.AppTheme, attrs)
        return ta.getDimension(0, 50f).toInt()
    }

    private fun checkPermission() {

        val permissionCheck = ContextCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION)

        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION)
        } else {
            needLocation = true
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {

        when (requestCode) {
            MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {

                    mMap?.isMyLocationEnabled = true
                    needLocation = true
                }
                return
            }
        }
    }

    override fun onDataUpdated(data: Item?) {

        if (data == null) {
            showError(Throwable("Место неизвестно"))
        } else if (!infoInitialized || !Objects.equals(data, this.data)) {

            this.data = data
            val item = data as MapItem

            if (infoInitialized) {
                notEnoughData = false
            }
            infoInitialized = true

            if (mapReady) {
                animateToMarker(item, false)
            }

            address.text = item.address
            metro.text = getString(R.string.metro_station, item.metro)
            description.text = item.description

            var visibility = if (item.thereIsWifi) View.VISIBLE else View.GONE
            wifi.visibility = visibility
            visibility = if (item.isAdapted) View.VISIBLE else View.GONE
            adaptation.visibility = visibility
            visibility = if (item.thereIsCanteen) View.VISIBLE else View.GONE
            canteen.visibility = visibility
            visibility = if (item.thereIsLibrary) View.VISIBLE else View.GONE
            library.visibility = visibility
            visibility = if (item.thereIsGym) View.VISIBLE else View.GONE
            gym.visibility = visibility
        }
    }

    override fun onMapReady(p0: GoogleMap?) {

        mapReady = true
        mMap = p0
        mMap?.setMinZoomPreference(10f)
        mMap?.setMaxZoomPreference(19f)
        mMap?.setInfoWindowAdapter(this)

        if (infoInitialized) {
            addAllMarkers()
            animateToMarker(data as MapItem, true)
        }

        mMap?.isMyLocationEnabled = needLocation
    }

    private fun animateToMarker(item: MapItem, justOpened: Boolean) {

        val location = LatLng(item.latitude, item.longitude)
        if (justOpened) {
            mMap?.moveCamera(CameraUpdateFactory.newLatLng(location))
            mMap?.moveCamera(CameraUpdateFactory.zoomTo(16f))
        } else {
            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(location, 16f), 700, null)
        }
    }

    private fun addAllMarkers() {

        val data = presenter.getAllData()
        data?.forEach {
            val location = LatLng(it.latitude, it.longitude)
            mMap?.addMarker(MarkerOptions().position(location).title(it.address))
        }
        mMap?.setOnMarkerClickListener {
            presenter.getData(it.title)?.let {
                infoInitialized = false
                val mapLatString = String.format("%.6f", mMap?.cameraPosition?.target?.latitude)
                val mapLonString = String.format("%.6f", mMap?.cameraPosition?.target?.longitude)
                val itemLatString = String.format("%.6f", (it as MapItem).latitude)
                val itemLonString = String.format("%.6f", it.longitude)
                if (mapLatString == itemLatString && mapLonString == itemLonString) {
                    return@setOnMarkerClickListener false
                }
                animateToMarker(it, false)
                onDataUpdated(it)
            }
            return@setOnMarkerClickListener true
        }
    }

    override fun resetMarkers() {

        mMap?.clear()
        addAllMarkers()
    }

    override fun getInfoContents(p0: Marker?): View {
        return View.inflate(context, R.layout.empty_view, null)
    }

    override fun getInfoWindow(p0: Marker?): View? {
        return View.inflate(context, R.layout.empty_view, null)
    }

    override fun onOldOrNullDataReceived() {

        if (notEnoughData) {
            showError(Throwable("Место неизвестно"))
        }
    }

    override fun backPressed(): Boolean {
        if (searchItem.isActionViewExpanded) {
            searchItem.collapseActionView()
            return true
        }
        if ((activity as MainActivity).backStackEntryCount == 0) {
            return false
        }
        (activity as MainActivity).selectNavigationItem(R.id.navigation_faculty)
        EventBus.getDefault().post(BackPressedEvent())
        return true
    }

    override fun showError(throwable: Throwable) {
        Log.e("MapError", throwable.message)
        if (notEnoughData && activity != null && isAdded) {
            Toast.makeText(activity.applicationContext, R.string.no_data_map, Toast.LENGTH_SHORT).show()
        }
    }

    override fun getData(): Item {
        return StringItem(receivedTitle)
    }
}