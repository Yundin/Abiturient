package org.styleru.abiturientproject.view.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import org.styleru.abiturientproject.R;
import org.styleru.abiturientproject.model.dto.EdProgramItem;
import org.styleru.abiturientproject.model.dto.Item;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class EdProgramsRecyclerAdapter extends BaseRecyclerAdapter {

    private final OnItemClick onItemClick;

    private final View.OnClickListener internalListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String title = (String) view.getTag();
            onItemClick.onItemClick(title);
        }
    };

    public EdProgramsRecyclerAdapter(Context context, List<Item> data, OnItemClick onItemClick) {
        super(context, data);
        this.onItemClick = onItemClick;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.ed_program_card, parent, false);
        return new EdProgramsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder uncastedHolder, int position) {
        EdProgramItem item = (EdProgramItem) getData().get(position);
        EdProgramsViewHolder holder = (EdProgramsViewHolder) uncastedHolder;

        holder.text.setText(item.getTitle());
        holder.itemView.setTag(item.getTitle());
        holder.itemView.setOnClickListener(internalListener);
    }

    class EdProgramsViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text)
        TextView text;

        EdProgramsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
