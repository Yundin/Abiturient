package org.styleru.abiturientproject.view;

/**
 * @author Yundin Vladislav
 */
public interface IOnFacultyTagClickListener {

    void onFacultyTagClick(String title);
}
