package org.styleru.abiturientproject.view;

import org.styleru.abiturientproject.model.dto.OrganizationItem;
import rx.observables.ConnectableObservable;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public interface IOnOrganizationClickListener {

    void onOrganizationClick(String title);

    void onOrganizationClick(String title, ConnectableObservable<List<OrganizationItem>> connectableObservable);
}
