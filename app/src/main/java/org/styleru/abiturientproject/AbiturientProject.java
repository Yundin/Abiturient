package org.styleru.abiturientproject;

import android.app.Application;
import com.yandex.metrica.YandexMetrica;
import org.styleru.abiturientproject.model.RepositoryProvider;
import org.styleru.abiturientproject.model.api.ApiFactory;
import org.styleru.abiturientproject.model.database.KotlinDBHelper;

/**
 * @author Yundin Vladislav
 */
public class AbiturientProject extends Application {

    public static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        KotlinDBHelper dbHelper = new KotlinDBHelper(getApplicationContext());
        dbHelper.getReadableDatabase().close();
        dbHelper.upgradeDataBase();

        YandexMetrica.activate(getApplicationContext(), "abbcf6ec-3364-4abf-ad41-5c27b90825bf");
        YandexMetrica.enableActivityAutoTracking(this);

        ApiFactory.recreate();
        RepositoryProvider.init(getApplicationContext());
    }
}
