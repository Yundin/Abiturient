package org.styleru.abiturientproject.model.database.parser

import org.jetbrains.anko.db.MapRowParser
import org.styleru.abiturientproject.model.database.HSEContract
import org.styleru.abiturientproject.model.dto.MapItem

/**
 * @author Yundin Vladislav
 */
class MapsRowParser : MapRowParser<MapItem> {
    override fun parseRow(columns: Map<String, Any?>): MapItem {
        return MapItem(columns[HSEContract.MapsEntry.COLUMN_ADDRESS] as String,
                columns[HSEContract.MapsEntry.COLUMN_METRO] as String,
                columns[HSEContract.MapsEntry.COLUMN_TIME] as String,
                columns[HSEContract.MapsEntry.COLUMN_DESCRIPTION] as String,
                columns[HSEContract.MapsEntry.COLUMN_WIFI] == 1L,
                columns[HSEContract.MapsEntry.COLUMN_ADAPTATION] == 1L,
                columns[HSEContract.MapsEntry.COLUMN_CANTEEN] == 1L,
                columns[HSEContract.MapsEntry.COLUMN_LIBRARY] == 1L,
                columns[HSEContract.MapsEntry.COLUMN_GYM] == 1L,
                columns[HSEContract.MapsEntry.COLUMN_LATITUDE] as Double,
                columns[HSEContract.MapsEntry.COLUMN_LONGITUDE] as Double)
    }
}