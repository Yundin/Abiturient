package org.styleru.abiturientproject.model.dto;

/**
 * @author Yundin Vladislav
 */
public class StringItem extends Item {

    private final String transferredData;

    public StringItem(String transferredData) {
        this.transferredData = transferredData;
    }

    public String getTransferredData() {
        return transferredData;
    }
}
