package org.styleru.abiturientproject.model.dto;

import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

import java.util.Objects;

/**
 * @author Yundin Vladislav
 */
public class SquareCardItem extends Item {

    @SerializedName("title")
    private final String title;
    @SerializedName("image")
    private final String imageUrl;

    public SquareCardItem(String title, @NotNull String imageUrl) {
        this.title = title;
        this.imageUrl = imageUrl;
    }

    @NotNull
    public String getImageUrl() {
        return imageUrl;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof SquareCardItem)) {
            return false;
        }
        SquareCardItem item = (SquareCardItem) obj;
        return item.title.equals(title) && item.imageUrl.equals(imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, imageUrl);
    }
}
