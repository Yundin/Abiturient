package org.styleru.abiturientproject.model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Yundin Vladislav
 */
public abstract class BaseResponse<T> {

    @SerializedName("response")
    private List<T> data;

    public List<T> getResponse() {

        if (data == null) {
            return new ArrayList<>();
        } else {
            return data;
        }
    }
}
