package org.styleru.abiturientproject.model.database;

import android.support.annotation.NonNull;
import org.styleru.abiturientproject.model.dto.*;
import org.styleru.abiturientproject.presenter.Presenter;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public interface DBRepository {

    List<Item> getEdProgramsDescriptionItems();

    List<Item> getEdProgramsDescriptionItemsByTitle(@NonNull String title);

    String getEdprogramAddressByTitle(@NonNull String title);

    String getEdprogramLinkByTitle(@NonNull String title);

    void updateEdProgramDescriptionItems(@NonNull List<EdProgramDescriptionItem> data, Presenter.IOnDataUpdatedCallback callback);

    List<Item> getEdProgramItems();

    List<Item> getEdProgramItemsByTag(@NonNull String tag);

    String getFacyltyTagImageByTitle(@NonNull String title);

    void updateEdProgramItems(@NonNull List<EdProgramItem> data, Presenter.IOnDataUpdatedCallback callback);

    Item getEventItemByTitle(String title);

    String getEventItemImageByTitle(@NonNull String title);

    List<Item> getEventItems();

    void updateEventItems(@NonNull List<EventItem> data, Presenter.IOnDataUpdatedCallback callback);

    Item getMapItemByAddress(String address);

    List<MapItem> getMapItems();

    void updateMapItems(@NonNull List<MapItem> data, Presenter.IOnDataUpdatedCallback callback);

    List<Item> getSquareCardItems(@NonNull String tableName, @NonNull String[] projection);

    void updateSquareCardItems(@NonNull String tableName, @NonNull List<SquareCardItem> data, @NonNull String[] projection, Presenter.IOnDataUpdatedCallback callback);

    List<Item> getOrganizationItems();

    List<Item> getOrganizationItemsByTag(@NonNull String title);

    Item getOrganizationItemByTitle(@NonNull String title);

    String getOrganizationTagImageByTitle(@NonNull String title);

    void updateOrganizationItems(@NonNull List<OrganizationItem> data, Presenter.IOnDataUpdatedCallback callback);
}
