package org.styleru.abiturientproject.model.api;

import org.styleru.abiturientproject.model.dto.*;
import org.styleru.abiturientproject.model.response.*;
import ru.arturvasilov.rxloader.RxUtils;
import rx.Observable;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public class DefaultAbiturientRepository implements AbiturientRepository {

    @Override
    public Observable<List<SquareCardItem>> getFacultiesTags() {
        return ApiFactory.getApiInterface()
                .getFacultiesTags()
                .map(FacultiesTagsResponse::getResponse)
                .compose(RxUtils.async());
    }

    @Override
    public Observable<List<EdProgramItem>> getEdPrograms() {
        return ApiFactory.getApiInterface()
                .getEdPrograms()
                .map(EdProgramsResponse::getResponse)
                .compose(RxUtils.async());
    }

    @Override
    public Observable<List<EdProgramDescriptionItem>> getEdProgramsDescription() {
        return ApiFactory.getApiInterface()
                .getEdProgramsDescription()
                .map(EdProgramDescriptionResponse::getResponse)
                .compose(RxUtils.async());
    }

    @Override
    public Observable<List<MapItem>> getMaps() {
        return ApiFactory.getApiInterface()
                .getMaps()
                .map(MapsResponse::getResponse)
                .compose(RxUtils.async());
    }

    @Override
    public Observable<List<SquareCardItem>> getOrganizationsTags() {
        return ApiFactory.getApiInterface()
                .getOrganizationsTags()
                .map(OrganizationsTagsResponse::getResponse)
                .compose(RxUtils.async());
    }

    @Override
    public Observable<List<OrganizationItem>> getOrganizations() {
        return ApiFactory.getApiInterface()
                .getOrganizations()
                .map(OrganizationsResponse::getResponse)
                .compose(RxUtils.async());
    }

    @Override
    public Observable<List<EventItem>> getEvents() {
        return ApiFactory.getApiInterface()
                .getEvents()
                .map(EventsResponse::getResponse)
                .compose(RxUtils.async());
    }
}
