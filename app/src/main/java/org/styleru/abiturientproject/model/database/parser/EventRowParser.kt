package org.styleru.abiturientproject.model.database.parser

import org.jetbrains.anko.db.MapRowParser
import org.styleru.abiturientproject.model.database.HSEContract
import org.styleru.abiturientproject.model.dto.EventItem
import org.styleru.abiturientproject.model.dto.Item

/**
 * @author Yundin Vladislav
 */
class EventRowParser : MapRowParser<Item> {
    override fun parseRow(columns: Map<String, Any?>): Item {
        return EventItem(columns[HSEContract.EventsEntry.COLUMN_TAG] as String,
                columns[HSEContract.EventsEntry.COLUMN_TITLE] as String,
                columns[HSEContract.EventsEntry.COLUMN_TEXT] as String,
                columns[HSEContract.EventsEntry.COLUMN_DATE] as String,
                columns[HSEContract.EventsEntry.COLUMN_LOCATION] as String,
                columns[HSEContract.EventsEntry.COLUMN_LINK] as String,
                columns[HSEContract.EventsEntry.COLUMN_IMAGE_URL] as String?)
    }
}