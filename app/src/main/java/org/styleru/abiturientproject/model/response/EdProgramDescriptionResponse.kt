package org.styleru.abiturientproject.model.response

import org.styleru.abiturientproject.model.dto.EdProgramDescriptionItem

/**
 * @author Yundin Vladislav
 */
class EdProgramDescriptionResponse : BaseResponse<EdProgramDescriptionItem>()