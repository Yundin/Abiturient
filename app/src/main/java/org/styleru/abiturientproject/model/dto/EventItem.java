package org.styleru.abiturientproject.model.dto;

import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * @author Yundin Vladislav
 */
public class EventItem extends Item {

    @SerializedName("tags")
    private final String tag;
    @SerializedName("title")
    private final String title;
    @SerializedName("text")
    private final String text;
    @SerializedName("created_date")
    private final String date;
    @SerializedName("location")
    private final String location;
    @SerializedName("link")
    private final String link;
    @Nullable
    @SerializedName("image")
    private final String image;

    public EventItem(String tag, String title, String text, String date, String location, String link, @Nullable String image) {
        this.tag = tag;
        this.title = title;
        this.text = text;
        this.date = date;
        this.location = location;
        this.link = link;
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public String getText() {
        return text;
    }

    public String getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public String getTag() {
        return tag;
    }

    public String getLink() {
        return link;
    }

    @Nullable
    public String getImage() {
        return image;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof EventItem)) {
            return false;
        }
        EventItem item = (EventItem) obj;
        return item.tag.equals(tag) && item.title.equals(title) && item.text.equals(text) && item.date.equals(date) && item.link.equals(link) && item.location.equals(location) && Objects.equals(item.image, image);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tag, title, text, date, link, location, image);
    }
}
