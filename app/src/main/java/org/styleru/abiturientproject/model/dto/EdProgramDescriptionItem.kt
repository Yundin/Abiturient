package org.styleru.abiturientproject.model.dto

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author Yundin Vladislav
 */
class EdProgramDescriptionItem constructor(edProgram: String, title: String, text: String) : Item() {

    @SerializedName("faculty")
    var edProgram = edProgram
        private set
    @SerializedName("title")
    var title = title
        private set
    @SerializedName("text")
    var text = text
        private set

    override fun equals(other: Any?): Boolean {
        if (other !is EdProgramDescriptionItem) return false
        return other.edProgram.equals(edProgram) && other.title.equals(title) && other.text.equals(text)
    }

    override fun hashCode(): Int {
        return Objects.hash(edProgram, title, text)
    }
}