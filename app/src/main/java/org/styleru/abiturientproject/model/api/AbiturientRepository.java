package org.styleru.abiturientproject.model.api;

import org.styleru.abiturientproject.model.dto.*;
import rx.Observable;

import java.util.List;

/**
 * @author Yundin Vladislav
 */
public interface AbiturientRepository {

    Observable<List<SquareCardItem>> getFacultiesTags();

    Observable<List<EdProgramItem>> getEdPrograms();

    Observable<List<EdProgramDescriptionItem>> getEdProgramsDescription();

    Observable<List<MapItem>> getMaps();

    Observable<List<SquareCardItem>> getOrganizationsTags();

    Observable<List<OrganizationItem>> getOrganizations();

    Observable<List<EventItem>> getEvents();
}
