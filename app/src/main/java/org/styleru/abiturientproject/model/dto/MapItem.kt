package org.styleru.abiturientproject.model.dto

import com.google.gson.annotations.SerializedName
import java.util.*

/**
 * @author Yundin Vladislav
 */
class MapItem(address: String, metro: String, timeOfWork: String, description: String, thereIsWifi: Boolean,
              isAdapted: Boolean, thereIsCanteen: Boolean, thereIsLibrary: Boolean, thereIsGym: Boolean,
              latitude: Double, longitude: Double) : Item() {

    @SerializedName("adress")
    var address = address
        private set
    @SerializedName("subway_station")
    var metro = metro
        private set
    @SerializedName("time_of_work")
    var timeOfWork = timeOfWork
        private set
    @SerializedName("description")
    var description = description
        private set
    @SerializedName("wifi")
    var thereIsWifi = thereIsWifi
        private set
    @SerializedName("adaptation_for_disabled_people")
    var isAdapted = isAdapted
        private set
    @SerializedName("canteen")
    var thereIsCanteen = thereIsCanteen
        private set
    @SerializedName("library")
    var thereIsLibrary = thereIsLibrary
        private set
    @SerializedName("gym")
    var thereIsGym = thereIsGym
        private set
    @SerializedName("latitude")
    var latitude = latitude
        private set
    @SerializedName("longitude")
    var longitude = longitude
        private set

    override fun equals(other: Any?): Boolean {
        if (other !is MapItem) return false
        return other.address.equals(address) && other.metro.equals(metro) && other.timeOfWork.equals(timeOfWork) &&
                other.description.equals(description) && other.thereIsWifi.equals(thereIsWifi) &&
                other.isAdapted.equals(isAdapted) && other.thereIsCanteen.equals(thereIsCanteen) &&
                other.thereIsLibrary.equals(thereIsLibrary) && other.thereIsGym.equals(thereIsGym) &&
                other.latitude.equals(latitude) && other.longitude.equals(longitude)
    }

    override fun hashCode(): Int {
        return Objects.hash(address, metro, timeOfWork, description, thereIsWifi, isAdapted, thereIsCanteen, thereIsLibrary, thereIsGym, latitude, longitude)
    }
}