package org.styleru.abiturientproject.model.database.parser

import org.jetbrains.anko.db.MapRowParser
import org.styleru.abiturientproject.model.database.HSEContract
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.model.dto.OrganizationItem

/**
 * @author Yundin Vladislav
 */
class OrganizationsRowParser : MapRowParser<Item> {
    override fun parseRow(columns: Map<String, Any?>): Item {
        return OrganizationItem(columns[HSEContract.OrganizationsEntry.COLUMN_TAG] as String,
                columns[HSEContract.OrganizationsEntry.COLUMN_TITLE] as String,
                columns[HSEContract.OrganizationsEntry.COLUMN_DESCRIPTION] as String,
                columns[HSEContract.OrganizationsEntry.COLUMN_LINK] as String,
                columns[HSEContract.OrganizationsEntry.COLUMN_IMAGE_URL] as String)
    }
}