package org.styleru.abiturientproject.model.database.parser

import org.jetbrains.anko.db.MapRowParser
import org.styleru.abiturientproject.model.database.HSEContract
import org.styleru.abiturientproject.model.dto.EdProgramItem
import org.styleru.abiturientproject.model.dto.Item

/**
 * @author Yundin Vladislav
 */
class EdProgramsRowParser : MapRowParser<Item> {
    override fun parseRow(columns: Map<String, Any?>): Item {
        return EdProgramItem(columns[HSEContract.EdProgramsEntry.COLUMN_TAG] as String,
                columns[HSEContract.EdProgramsEntry.COLUMN_TITLE] as String,
                columns[HSEContract.EdProgramsEntry.COLUMN_LOCATION] as String,
                columns[HSEContract.EdProgramsEntry.COLUMN_LINK] as String)
    }
}