package org.styleru.abiturientproject.model;


import android.content.Context;
import android.support.annotation.MainThread;
import android.support.annotation.NonNull;
import org.jetbrains.annotations.Contract;
import org.styleru.abiturientproject.model.api.AbiturientRepository;
import org.styleru.abiturientproject.model.api.DefaultAbiturientRepository;
import org.styleru.abiturientproject.model.database.DBRepository;
import org.styleru.abiturientproject.model.database.DefaultDBRepository;

/**
 * @author Yundin Vladislav
 */
public class RepositoryProvider {

    private static AbiturientRepository abiturientRepository;
    private static DBRepository databaseRepository;

    public static AbiturientRepository provideAbiturientRepository() {
        if (abiturientRepository == null) {
            abiturientRepository = new DefaultAbiturientRepository();
        }
        return abiturientRepository;
    }

    public static void setAbiturientRepository(@NonNull AbiturientRepository repository) {
        abiturientRepository = repository;
    }

    @Contract(pure = true)
    public static DBRepository provideDBRepository() {
        return databaseRepository;
    }

    public static void setDBRepository(@NonNull DBRepository repository) {
        databaseRepository = repository;
    }

    @MainThread
    public static void init(Context context) {
        abiturientRepository = new DefaultAbiturientRepository();
        databaseRepository = new DefaultDBRepository(context);
    }
}
