package org.styleru.abiturientproject.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * @author Yundin Vladislav
 */
public class EdProgramItem extends Item {

    @SerializedName("tag")
    private final String facultyTag;
    @SerializedName("title")
    private final String title;
    @SerializedName("location")
    private final String location;
    @SerializedName("link")
    private final String link;

    public EdProgramItem(String facultyTag, String title, String location, String link) {
        this.facultyTag = facultyTag;
        this.title = title;
        this.location = location;
        this.link = link;
    }

    public String getFacultyTag() {
        return facultyTag;
    }

    public String getTitle() {
        return title;
    }

    public String getLocation() {
        return location;
    }

    public String getLink() {
        return link;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof EdProgramItem)) {
            return false;
        }
        EdProgramItem item = (EdProgramItem) obj;
        return item.facultyTag.equals(facultyTag) && item.title.equals(title) && item.location.equals(location) && item.link.equals(link);
    }

    @Override
    public int hashCode() {
        return Objects.hash(facultyTag, title, location, link);
    }

}
