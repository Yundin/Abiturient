package org.styleru.abiturientproject.model.response;

import org.styleru.abiturientproject.model.dto.EventItem;

/**
 * @author Yundin Vladislav
 */
public class EventsResponse extends BaseResponse<EventItem> {

}
