package org.styleru.abiturientproject.model.database.parser

import org.jetbrains.anko.db.MapRowParser
import org.styleru.abiturientproject.model.dto.Item
import org.styleru.abiturientproject.model.dto.SquareCardItem

/**
 * @author Yundin Vladislav
 */
class SquareCardsRowParser(val projection: Array<String>) : MapRowParser<Item> {

    override fun parseRow(columns: Map<String, Any?>): Item {
        return SquareCardItem(columns[projection[0]] as String,
                columns[projection[1]] as String)
    }
}