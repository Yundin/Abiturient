package org.styleru.abiturientproject.model.api;

import org.styleru.abiturientproject.model.response.*;
import retrofit2.http.GET;
import rx.Observable;

/**
 * @author Yundin Vladislav
 */
public interface ApiInterface {

    @GET("faculties/faculties_tags")
    Observable<FacultiesTagsResponse> getFacultiesTags();

    @GET("faculties/faculties")
    Observable<EdProgramsResponse> getEdPrograms();

    @GET("faculties/faculties_tags_inside")
    Observable<EdProgramDescriptionResponse> getEdProgramsDescription();

    @GET("faculties/locations_info")
    Observable<MapsResponse> getMaps();

    @GET("organizations/organizations_tags")
    Observable<OrganizationsTagsResponse> getOrganizationsTags();

    @GET("organizations/organizations")
    Observable<OrganizationsResponse> getOrganizations();

    @GET("news/news")
    Observable<EventsResponse> getEvents();

}
