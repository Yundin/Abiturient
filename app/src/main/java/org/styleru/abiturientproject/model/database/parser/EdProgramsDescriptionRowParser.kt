package org.styleru.abiturientproject.model.database.parser

import org.jetbrains.anko.db.MapRowParser
import org.styleru.abiturientproject.model.database.HSEContract
import org.styleru.abiturientproject.model.dto.EdProgramDescriptionItem
import org.styleru.abiturientproject.model.dto.Item

/**
 * @author Yundin Vladislav
 */
class EdProgramsDescriptionRowParser : MapRowParser<Item> {
    override fun parseRow(columns: Map<String, Any?>): Item {
        return EdProgramDescriptionItem(columns[HSEContract.EdProgramsDescriptionEntry.COLUMN_PROGRAM] as String,
                columns[HSEContract.EdProgramsDescriptionEntry.COLUMN_TITLE] as String,
                columns[HSEContract.EdProgramsDescriptionEntry.COLUMN_TEXT] as String)
    }
}