package org.styleru.abiturientproject.model.response;

import org.styleru.abiturientproject.model.dto.MapItem;

/**
 * @author Yundin Vladislav
 */
public class MapsResponse extends BaseResponse<MapItem> {
}
