package org.styleru.abiturientproject.model.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.ManagedSQLiteOpenHelper
import org.styleru.abiturientproject.BuildConfig
import java.io.*

/**
 * @author Yundin Vladislav
 */
class KotlinDBHelper(ctx: Context) : ManagedSQLiteOpenHelper(ctx, dBName, null, dBVersion) {
    companion object {
        private var instance: KotlinDBHelper? = null
        private const val dBName = "hse.db"
        private const val dBVersion = 5

        @Synchronized
        fun getInstance(ctx: Context): KotlinDBHelper {
            if (instance == null) {
                instance = KotlinDBHelper(ctx.applicationContext)
            }
            return instance!!
        }
    }

    var context: Context = ctx
    private val dBPath: String
    private var needUpdate = false

    init {
        dBPath = context.applicationInfo.dataDir + "/databases/"
    }

    fun upgradeDataBase() {
        if (needUpdate) {
            val dbFile = File(dBPath + dBName)
            if (dbFile.exists()) {
                dbFile.delete()
            }
            copyDataBase()
            needUpdate = false
        }
    }

    private fun checkDataBase(): Boolean {

        val dbFile = File(dBPath + dBName)
        return dbFile.exists()
    }

    private fun copyDataBase() {

        if (!checkDataBase()) {
            try {
                copyDBFile()
            } catch (e: IOException) {
                if (BuildConfig.DEBUG) {
                    e.printStackTrace()
                }
            }

        }
    }

    @Throws(IOException::class)
    private fun copyDBFile() {
        val myInput: InputStream = context.assets.open("databases/$dBName")
        val f = File(dBPath)
        if (!f.exists()) {
            f.mkdir()
        }
        val outFileName = dBPath + dBName
        val myOutput: OutputStream = FileOutputStream(outFileName)
        val buffer = ByteArray(1024)
        var length: Int
        do {
            length = myInput.read(buffer)
            if (length <= 0) {
                break
            }
            myOutput.write(buffer, 0, length)
        } while (true)
        myOutput.flush()
        myOutput.close()
        myInput.close()

    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        needUpdate = newVersion > oldVersion
    }

    override fun onDowngrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        //do nothing
    }

    override fun onCreate(db: SQLiteDatabase?) {
        needUpdate = true
    }
}

// Access property for Context
val Context.database: KotlinDBHelper
    get() = KotlinDBHelper.getInstance(applicationContext)