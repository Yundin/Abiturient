package org.styleru.abiturientproject.model.database

import android.content.Context
import kotlinx.coroutines.experimental.Deferred
import kotlinx.coroutines.experimental.android.UI
import kotlinx.coroutines.experimental.async
import org.jetbrains.anko.coroutines.experimental.bg
import org.jetbrains.anko.db.MapRowParser
import org.jetbrains.anko.db.StringParser
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.select
import org.styleru.abiturientproject.BuildConfig
import org.styleru.abiturientproject.model.database.parser.*
import org.styleru.abiturientproject.model.dto.*
import org.styleru.abiturientproject.presenter.Presenter

/**
 * @author Yundin Vladislav
 */
class DefaultDBRepository(context: Context) : DBRepository {

    private val database: KotlinDBHelper = KotlinDBHelper(context)

    private fun defaultGetList(tableName: String, parser: MapRowParser<Item>): List<Item>? {
        var data: List<Item>? = null
        try {
            database.use {
                data = this.select(tableName)
                        .parseList(parser)
            }
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }
        return data
    }

    private fun defaultGetListByTitle(tableName: String, parser: MapRowParser<Item>, titleColumn: String, title: String): List<Item>? {
        var data: List<Item>? = null
        try {
            database.use {
                data = this.select(tableName)
                        .whereSimple("$titleColumn = \"$title\"")
                        .parseList(parser)
            }
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }
        return data
    }

    private fun defaultGetStringByTitle(tableName: String, neededColumn: String, titleColumn: String, title: String): String? {
        var data: String? = null
        try {
            database.use {
                data = this.select(tableName, neededColumn)
                        .whereSimple("$titleColumn = \"$title\"")
                        .parseSingle(StringParser)
            }
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }
        return data
    }

    private fun defaultGetItemByTitle(tableName: String, parser: MapRowParser<Item>, titleColumn: String, title: String): Item? {
        var data: Item? = null
        try {
            database.use {
                data = this.select(tableName)
                        .whereSimple("$titleColumn = \"$title\"")
                        .parseSingle(parser)
            }
        } catch (e: Exception) {
            if (BuildConfig.DEBUG) {
                e.printStackTrace()
            }
        }
        return data
    }

    private fun defaultUpdate(tableName: String, valuesList: MutableList<Array<Pair<String, Any?>>>, callback: Presenter.IOnDataUpdatedCallback?) {
        async(UI) {
            val isUpdated: Deferred<Boolean> = bg {
                database.use {
                    delete(tableName, null, null)
                    valuesList.forEach {
                        insert(tableName,
                                *it)
                    }
                }
                return@bg true
            }
            if (isUpdated.await()) {
                callback?.onDataUpdated()
            }
        }
    }

    override fun getEdProgramsDescriptionItems(): List<Item>? {
        return defaultGetList(HSEContract.EdProgramsDescriptionEntry.TABLE_NAME, EdProgramsDescriptionRowParser())
    }

    override fun getEdProgramsDescriptionItemsByTitle(title: String): List<Item>? {
        return defaultGetListByTitle(HSEContract.EdProgramsDescriptionEntry.TABLE_NAME, EdProgramsDescriptionRowParser(),
                HSEContract.EdProgramsDescriptionEntry.COLUMN_PROGRAM, title)
    }

    override fun getEdprogramAddressByTitle(title: String): String? {
        return defaultGetStringByTitle(HSEContract.EdProgramsEntry.TABLE_NAME, HSEContract.EdProgramsEntry.COLUMN_LOCATION,
                HSEContract.EdProgramsEntry.COLUMN_TITLE, title)
    }

    override fun getEdprogramLinkByTitle(title: String): String? {
        return defaultGetStringByTitle(HSEContract.EdProgramsEntry.TABLE_NAME, HSEContract.EdProgramsEntry.COLUMN_LINK,
                HSEContract.EdProgramsEntry.COLUMN_TITLE, title)
    }

    override fun updateEdProgramDescriptionItems(data: List<EdProgramDescriptionItem>, callback: Presenter.IOnDataUpdatedCallback?) {
        val valuesList: MutableList<Array<Pair<String, Any?>>> = ArrayList()
        data.forEach {
            valuesList.add(arrayOf(HSEContract.EdProgramsDescriptionEntry.COLUMN_PROGRAM to it.edProgram,
                    HSEContract.EdProgramsDescriptionEntry.COLUMN_TITLE to it.title,
                    HSEContract.EdProgramsDescriptionEntry.COLUMN_TEXT to it.text))
        }
        defaultUpdate(HSEContract.EdProgramsDescriptionEntry.TABLE_NAME, valuesList, callback)
    }

    override fun getEdProgramItems(): List<Item>? {
        return defaultGetList(HSEContract.EdProgramsEntry.TABLE_NAME, EdProgramsRowParser())
    }

    override fun getEdProgramItemsByTag(tag: String): List<Item>? {
        return defaultGetListByTitle(HSEContract.EdProgramsEntry.TABLE_NAME, EdProgramsRowParser(),
                HSEContract.EdProgramsEntry.COLUMN_TAG, tag)
    }

    override fun getFacyltyTagImageByTitle(title: String): String? {
        return defaultGetStringByTitle(HSEContract.FacultiesTagsEntry.TABLE_NAME, HSEContract.FacultiesTagsEntry.COLUMN_IMAGE_URL,
                HSEContract.FacultiesTagsEntry.COLUMN_TITLE, title)
    }

    override fun updateEdProgramItems(data: List<EdProgramItem>, callback: Presenter.IOnDataUpdatedCallback?) {
        val valuesList: MutableList<Array<Pair<String, Any?>>> = ArrayList()
        data.forEach {
            valuesList.add(arrayOf(HSEContract.EdProgramsEntry.COLUMN_TAG to it.facultyTag,
                    HSEContract.EdProgramsEntry.COLUMN_TITLE to it.title,
                    HSEContract.EdProgramsEntry.COLUMN_LOCATION to it.location,
                    HSEContract.EdProgramsEntry.COLUMN_LINK to it.link))
        }
        defaultUpdate(HSEContract.EdProgramsEntry.TABLE_NAME, valuesList, callback)
    }

    override fun getEventItemByTitle(title: String): Item? {
        return defaultGetItemByTitle(HSEContract.EventsEntry.TABLE_NAME, EventRowParser(),
                HSEContract.EventsEntry.COLUMN_TITLE, title)
    }

    override fun getEventItemImageByTitle(title: String): String? {
        return defaultGetStringByTitle(HSEContract.EventsEntry.TABLE_NAME, HSEContract.EventsEntry.COLUMN_IMAGE_URL,
                HSEContract.EventsEntry.COLUMN_TITLE, title)
    }

    override fun getEventItems(): List<Item>? {
        return defaultGetList(HSEContract.EventsEntry.TABLE_NAME, EventRowParser())
    }

    override fun updateEventItems(data: List<EventItem>, callback: Presenter.IOnDataUpdatedCallback?) {
        val valuesList: MutableList<Array<Pair<String, Any?>>> = ArrayList()
        data.forEach {
            valuesList.add(arrayOf(HSEContract.EventsEntry.COLUMN_TAG to it.tag,
                    HSEContract.EventsEntry.COLUMN_TITLE to it.title,
                    HSEContract.EventsEntry.COLUMN_TEXT to it.text,
                    HSEContract.EventsEntry.COLUMN_DATE to it.date,
                    HSEContract.EventsEntry.COLUMN_LOCATION to it.location,
                    HSEContract.EventsEntry.COLUMN_LINK to it.link,
                    HSEContract.EventsEntry.COLUMN_IMAGE_URL to it.image))
        }
        defaultUpdate(HSEContract.EventsEntry.TABLE_NAME, valuesList, callback)
    }

    override fun getMapItemByAddress(address: String): Item? {
        return defaultGetItemByTitle(HSEContract.MapsEntry.TABLE_NAME, MapsRowParser(), HSEContract.MapsEntry.COLUMN_ADDRESS,
                address)
    }

    override fun getMapItems(): List<MapItem>? {
        return defaultGetList(HSEContract.MapsEntry.TABLE_NAME, MapsRowParser())
                ?.filterIsInstance<MapItem>()
    }

    override fun updateMapItems(data: List<MapItem>, callback: Presenter.IOnDataUpdatedCallback?) {
        val valuesList: MutableList<Array<Pair<String, Any?>>> = ArrayList()
        data.forEach {
            valuesList.add(arrayOf(HSEContract.MapsEntry.COLUMN_ADDRESS to it.address,
                    HSEContract.MapsEntry.COLUMN_METRO to it.metro,
                    HSEContract.MapsEntry.COLUMN_TIME to it.timeOfWork,
                    HSEContract.MapsEntry.COLUMN_DESCRIPTION to it.description,
                    HSEContract.MapsEntry.COLUMN_WIFI to (if (it.thereIsWifi) 1 else 0),
                    HSEContract.MapsEntry.COLUMN_ADAPTATION to (if (it.isAdapted) 1 else 0),
                    HSEContract.MapsEntry.COLUMN_CANTEEN to (if (it.thereIsCanteen) 1 else 0),
                    HSEContract.MapsEntry.COLUMN_LIBRARY to (if (it.thereIsLibrary) 1 else 0),
                    HSEContract.MapsEntry.COLUMN_GYM to (if (it.thereIsGym) 1 else 0),
                    HSEContract.MapsEntry.COLUMN_LATITUDE to it.latitude,
                    HSEContract.MapsEntry.COLUMN_LONGITUDE to it.longitude))
        }
        defaultUpdate(HSEContract.MapsEntry.TABLE_NAME, valuesList, callback)
    }

    override fun getSquareCardItems(tableName: String, projection: Array<String>): List<Item>? {
        return defaultGetList(tableName, SquareCardsRowParser(projection))
    }

    override fun updateSquareCardItems(tableName: String, data: List<SquareCardItem>, projection: Array<String>, callback: Presenter.IOnDataUpdatedCallback?) {
        val valuesList: MutableList<Array<Pair<String, Any?>>> = ArrayList()
        data.forEach {
            valuesList.add(arrayOf(projection[0] to it.title,
                    projection[1] to it.imageUrl))
        }
        defaultUpdate(tableName, valuesList, callback)
    }

    override fun getOrganizationItems(): List<Item>? {
        return defaultGetList(HSEContract.OrganizationsEntry.TABLE_NAME, OrganizationsRowParser())
    }

    override fun getOrganizationItemsByTag(tag: String): List<Item>? {
        return defaultGetListByTitle(HSEContract.OrganizationsEntry.TABLE_NAME, OrganizationsRowParser(),
                HSEContract.OrganizationsEntry.COLUMN_TAG, tag)
    }

    override fun getOrganizationItemByTitle(title: String): Item? {
        return defaultGetItemByTitle(HSEContract.OrganizationsEntry.TABLE_NAME, OrganizationsRowParser(),
                HSEContract.OrganizationsEntry.COLUMN_TITLE, title)
    }

    override fun getOrganizationTagImageByTitle(title: String): String? {
        return defaultGetStringByTitle(HSEContract.OrganizationsTagsEntry.TABLE_NAME, HSEContract.OrganizationsTagsEntry.COLUMN_IMAGE_URL,
                HSEContract.OrganizationsTagsEntry.COLUMN_TITLE, title)
    }

    override fun updateOrganizationItems(data: List<OrganizationItem>, callback: Presenter.IOnDataUpdatedCallback?) {
        val valuesList: MutableList<Array<Pair<String, Any?>>> = ArrayList()
        data.forEach {
            valuesList.add(arrayOf(HSEContract.OrganizationsEntry.COLUMN_TAG to it.organizationTag,
                    HSEContract.OrganizationsEntry.COLUMN_TITLE to it.title,
                    HSEContract.OrganizationsEntry.COLUMN_DESCRIPTION to it.description,
                    HSEContract.OrganizationsEntry.COLUMN_LINK to it.link,
                    HSEContract.OrganizationsEntry.COLUMN_IMAGE_URL to it.imgUrl))
        }
        return defaultUpdate(HSEContract.OrganizationsEntry.TABLE_NAME, valuesList, callback)
    }
}