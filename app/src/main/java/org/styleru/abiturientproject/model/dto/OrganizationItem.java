package org.styleru.abiturientproject.model.dto;


import android.support.annotation.Nullable;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

/**
 * @author Yundin Vladislav
 */
public class OrganizationItem extends Item {

    @SerializedName("tag")
    private final String organizationTag;
    @SerializedName("title")
    private final String title;
    @SerializedName("description")
    private final String description;
    @SerializedName("link")
    private final String link;
    @Nullable
    @SerializedName("image")
    private final String imgUrl;

    public OrganizationItem(String organizationTag, String title, String description, String link, @Nullable String imgUrl) {
        this.organizationTag = organizationTag;
        this.title = title;
        this.description = description;
        this.link = link;
        this.imgUrl = imgUrl;
    }

    public String getOrganizationTag() {
        return organizationTag;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLink() {
        return link;
    }

    @Nullable
    public String getImgUrl() {
        return imgUrl;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof OrganizationItem)) {
            return false;
        }
        OrganizationItem item = (OrganizationItem) obj;
        return item.organizationTag.equals(organizationTag) && item.title.equals(title) && item.description.equals(description) && item.link.equals(link) && Objects.equals(item.imgUrl, imgUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(organizationTag, title, description, link, imgUrl);
    }
}
