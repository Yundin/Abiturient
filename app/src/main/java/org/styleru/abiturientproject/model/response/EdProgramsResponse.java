package org.styleru.abiturientproject.model.response;

import org.styleru.abiturientproject.model.dto.EdProgramItem;

/**
 * @author Yundin Vladislav
 */
public class EdProgramsResponse extends BaseResponse<EdProgramItem> {

}
