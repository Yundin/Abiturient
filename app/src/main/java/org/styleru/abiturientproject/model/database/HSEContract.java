package org.styleru.abiturientproject.model.database;

import android.provider.BaseColumns;

/**
 * @author Yundin Vladislav
 */
public final class HSEContract {

    private HSEContract() {
    }

    public static final class EventsEntry implements BaseColumns {
        public static final String TABLE_NAME = "events";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_TAG = "tag";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_TEXT = "text";
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_LOCATION = "location";
        public static final String COLUMN_LINK = "link";
        public static final String COLUMN_IMAGE_URL = "image";
    }

    public static final class FacultiesTagsEntry implements BaseColumns {
        public static final String TABLE_NAME = "faculties_tags";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_IMAGE_URL = "image";
    }

    public static final class EdProgramsEntry implements BaseColumns {
        public static final String TABLE_NAME = "ed_programs";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_TAG = "tag";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_LINK = "link";
        public static final String COLUMN_LOCATION = "location";
    }

    public static final class EdProgramsDescriptionEntry implements BaseColumns {
        public static final String TABLE_NAME = "ed_programs_description";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_PROGRAM = "faculty";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_TEXT = "text";
    }

    public static final class MapsEntry implements BaseColumns {
        public static final String TABLE_NAME = "maps_info";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_ADDRESS = "address";
        public static final String COLUMN_METRO = "metro";
        public static final String COLUMN_TIME = "time_of_work";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_WIFI = "wifi";
        public static final String COLUMN_ADAPTATION = "adaptation";
        public static final String COLUMN_CANTEEN = "canteen";
        public static final String COLUMN_LIBRARY = "library";
        public static final String COLUMN_GYM = "gym";
        public static final String COLUMN_LATITUDE = "latitude";
        public static final String COLUMN_LONGITUDE = "longitude";
    }

    public static final class OrganizationsTagsEntry implements BaseColumns {
        public static final String TABLE_NAME = "organizations_tags";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_IMAGE_URL = "image";
    }

    public static final class OrganizationsEntry implements BaseColumns {
        public static final String TABLE_NAME = "organizations";

        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_TAG = "tag";
        public static final String COLUMN_TITLE = "title";
        public static final String COLUMN_DESCRIPTION = "description";
        public static final String COLUMN_LINK = "link";
        public static final String COLUMN_IMAGE_URL = "image";
    }
}
