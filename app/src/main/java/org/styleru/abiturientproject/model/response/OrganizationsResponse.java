package org.styleru.abiturientproject.model.response;

import org.styleru.abiturientproject.model.dto.OrganizationItem;

/**
 * @author Yundin Vladislav
 */
public class OrganizationsResponse extends BaseResponse<OrganizationItem> {

}